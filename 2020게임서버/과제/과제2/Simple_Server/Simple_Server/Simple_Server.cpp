#include <iostream>
#include <WS2tcpip.h>				// 윈도우 소켓 사용
using namespace std;

#pragma comment(lib, "Ws2_32.lib")	
constexpr int BUF_SIZE = 1024;
constexpr short PORT = 3500;

//#pragma warning(disable : 4996)		// WSASocketW 안써서 생긴 오류

// 키입력 확인용
const DWORD KEY_UP = 0x00000001;
const DWORD KEY_DOWN = 0x00000002;
const DWORD KEY_LEFT = 0x00000004;
const DWORD KEY_RIGHT = 0x00000008;
const DWORD KEY_ESCAPE = 0x00000010;

struct INFO
{
	float moveX;
	float moveY;
};

constexpr float sizeX = (800 / 8);
constexpr float sizeY = (600 / 8);

void error_display(const char* msg, int err_no)
{
	WCHAR* lpMsgBuf;
	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, err_no, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPWSTR)&lpMsgBuf, 0, NULL);
	cout << msg;
	wcout << L"  에러 => " << lpMsgBuf << endl;
	while (true);
	LocalFree(lpMsgBuf);
}

int main()
{
	wcout.imbue(std::locale("korean"));
	WSADATA WSAData;
	WSAStartup(MAKEWORD(2, 0), &WSAData);	// WSACleanup과 쌍을 이룸, 윈속 사용을 알려주는 것, 무조건 이렇게 한다 그냥

	SOCKET serverSocket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, 0);		// 서버와 연결하는 소켓 생성
	SOCKADDR_IN serverAddr;		// 서버 주소 선언
	memset(&serverAddr, 0, sizeof(serverAddr));	// 반드시 모두 0으로 초기화 해줘야한다. 
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);
	serverAddr.sin_addr.s_addr = INADDR_ANY;
	::bind(serverSocket, (sockaddr*)&serverAddr, sizeof(serverAddr));
	listen(serverSocket, SOMAXCONN);

	while (true)
	{
		SOCKADDR_IN clientAddr;
		INT addr_size = sizeof(clientAddr);
		SOCKET clientSocket = WSAAccept(serverSocket, (sockaddr*)&clientAddr, &addr_size, NULL, NULL);
		if (SOCKET_ERROR == clientSocket)
		{
			error_display("WSAAccept", WSAGetLastError());
		}
		cout << "New client accepted.\n";
		while (true)
		{
			char messageBuffer[BUF_SIZE + 1];			// NULL문자 까지 더해줌
			WSABUF wsabuf;
			wsabuf.buf = messageBuffer;
			wsabuf.len = BUF_SIZE;		// 얼마가 올지 모르기 때문에 최대 사이즈만큼 지정

			// 받기
			DWORD num_recv;
			DWORD flag = 0;
			WSARecv(clientSocket, &wsabuf, 1, &num_recv, &flag, NULL, NULL);
			//cout << "Received " << num_recv << " Bytes [" << wsabuf.buf << "]\n";

			if (0 == num_recv)
				break;

			INFO playerMove;
			playerMove.moveX = 0;
			playerMove.moveY = 0;


			if (memcmp(&KEY_UP, wsabuf.buf, strlen(wsabuf.buf)) == 0)
			{
				cout << " 위로 이동" << endl;
				playerMove.moveX = 0;
				playerMove.moveY = -sizeY;
			}

			if (memcmp(&KEY_DOWN, wsabuf.buf, strlen(wsabuf.buf)) == 0)
			{
				cout << " 아래로 이동" << endl;
				playerMove.moveX = 0;
				playerMove.moveY = sizeY;
			}

			if (memcmp(&KEY_LEFT, wsabuf.buf, strlen(wsabuf.buf)) == 0)
			{
				cout << " 왼쪽으로 이동" << endl;
				playerMove.moveX = -sizeX;
				playerMove.moveY = 0;
			}

			if (memcmp(&KEY_RIGHT, wsabuf.buf, strlen(wsabuf.buf)) == 0)
			{
				cout << " 오른쪽으로 이동" << endl;
				playerMove.moveX = sizeX;
				playerMove.moveY = 0;
			}

			if (memcmp(&KEY_ESCAPE, wsabuf.buf, strlen(wsabuf.buf)) == 0)
			{
				cout << " 종료 " << endl;
				break;
			}


			// 보내기
			DWORD num_sent;	// 전송된 길이 반환용도
			memcpy(wsabuf.buf, &playerMove, sizeof(INFO));
			wsabuf.len = BUF_SIZE;
			int ret = WSASend(clientSocket, &wsabuf, 1, &num_sent, 0, NULL, NULL);		// 버퍼 1개만 보내니까 1
			if (SOCKET_ERROR == ret) {
				error_display("WSASend", WSAGetLastError());
			}
			//cout << "Sent " << wsabuf.len << " Bytes [" << messageBuffer << "]\n";

		}
		cout << "Client connection closed\n";
		closesocket(clientSocket);
	}

	closesocket(serverSocket);
	WSACleanup();
}