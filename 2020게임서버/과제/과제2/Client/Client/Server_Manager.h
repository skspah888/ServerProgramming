#pragma once
class Server_Manager
{
	DECLARE_SINGLETON(Server_Manager)

	struct INFO
	{
		float moveX;
		float moveY;
	};

private:
	explicit Server_Manager();
	~Server_Manager();

public:
	const INFO& GetMoveInfo() { return playerMove; }

public:
	HRESULT			Initialize();		// 클라-서버 초기화
	HRESULT			Update();			// 서버 업데이트 [ 서버에 데이터 전송 ]
	HRESULT			ReceiveData(const DWORD KeyInput);		// 서버에 데이터 전송

private:
	WSADATA			WSAData;
	SOCKET			serverSocket;		// 서버와 연결하는 소켓
	SOCKADDR_IN		serverAddr;	

private:
	INFO			playerMove;
};

