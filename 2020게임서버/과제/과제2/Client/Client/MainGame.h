#pragma once
class ChessPawn;
class MainGame
{
public:
	explicit MainGame();
	~MainGame();

public:
	HRESULT		Ready_MainGame();
	HRESULT		Update_MainGame(double TimeDeleta);
	HRESULT		Render_MainGame();

private:
	HDC			m_hDC;
	MapInfo		m_ChessMap[MAP_SIZEX][MAP_SIZEY];
	ChessPawn*	m_pChessPawn = nullptr;


private:
	Key_Manager* m_pKey_Manager = nullptr;
	Server_Manager* m_pServer_Manager = nullptr;

public:
	static MainGame*	Create();
	void				Release();
};

