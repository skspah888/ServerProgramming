﻿// header.h: 표준 시스템 포함 파일
// 또는 프로젝트 특정 포함 파일이 들어 있는 포함 파일입니다.
//

#pragma once

#include "targetver.h"
#define WIN32_LEAN_AND_MEAN             // 거의 사용되지 않는 내용을 Windows 헤더에서 제외합니다.
// Windows 헤더 파일
#include <windows.h>
// C 런타임 헤더 파일입니다.
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <vector>
#include <list>
#include <algorithm>
#include <functional>
#include <iostream>
#include <WS2tcpip.h>
using namespace std;

#pragma comment(lib, "Ws2_32.lib")	// 
constexpr int BUF_SIZE = 1024;
constexpr short PORT = 3500;

#include "Extern.h"
#include "Macro.h"
#include "Struct.h"


#include "Key_Manager.h"
#include "Server_Manager.h"


#ifdef UNICODE
#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console")
#else
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console")
#endif