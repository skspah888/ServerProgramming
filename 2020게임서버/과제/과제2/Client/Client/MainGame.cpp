#include "framework.h"
#include "MainGame.h"
#include "ChessPawn.h"

MainGame::MainGame()
{
}

MainGame::~MainGame()
{
    Release();
}

HRESULT MainGame::Ready_MainGame()
{
    m_hDC = GetDC(g_hWnd);
    m_pKey_Manager = Key_Manager::GetInstance();
	m_pServer_Manager = Server_Manager::GetInstance();

    // 체스판 초기화
    for (int i = 0; i < MAP_SIZEX; ++i)
    {
        for (int j = 0; j < MAP_SIZEY; ++j)
        {
            m_ChessMap[j][i].size.x = (g_iWinCX / MAP_SIZEX);       // 100
            m_ChessMap[j][i].size.y = (g_iWinCY / MAP_SIZEY);       // 75

            m_ChessMap[j][i].center.x = j * (g_iWinCX / MAP_SIZEX) + m_ChessMap[j][i].size.x / 2;
            m_ChessMap[j][i].center.y = i * (g_iWinCY / MAP_SIZEY) + m_ChessMap[j][i].size.y / 2;
        }
    }

	// 서버 초기화
	if (FAILED(m_pServer_Manager->Initialize()))
	{
		return E_FAIL;
	};

	// 체스말 생성
	m_pChessPawn = ChessPawn::Create();

    return S_OK;
}

HRESULT MainGame::Update_MainGame(double TimeDeleta)
{
    m_pKey_Manager->CheckKeyInput();
	if (-1 == m_pChessPawn->Update_GameObject(TimeDeleta))
		return E_FAIL;
    return S_OK;
}

HRESULT MainGame::Render_MainGame()
{
	Rectangle(m_hDC, 0, 0, g_iWinCX * 2, g_iWinCY * 2);

	/////////////////////////////////////////////////////////////////
	 // 체스판 
	for (int i = 0; i < MAP_SIZEY; ++i)
	{
		for (int j = 0; j < MAP_SIZEX; ++j)
		{
			HPEN hPen = CreatePen(PS_SOLID, 1, RGB(25, 75, 125));
			HPEN hOldPen = (HPEN)SelectObject(m_hDC, hPen);
			HBRUSH hBrush = (HBRUSH)GetStockObject(NULL_BRUSH);
			HBRUSH hOldBrush = (HBRUSH)SelectObject(m_hDC, hBrush);

			Rectangle(m_hDC,
				m_ChessMap[j][i].center.x - (m_ChessMap[j][i].size.x / 2),
				m_ChessMap[j][i].center.y - (m_ChessMap[j][i].size.y / 2),
				m_ChessMap[j][i].center.x + (m_ChessMap[j][i].size.x / 2),
				m_ChessMap[j][i].center.y + (m_ChessMap[j][i].size.y / 2));

			SelectObject(m_hDC, hOldBrush);
			DeleteObject(hBrush);
			SelectObject(m_hDC, hOldPen);
			DeleteObject(hPen);
		}
	}

	m_pChessPawn->Render_GameObject(m_hDC);

	////////////////////////////////////////////////////////////

    return S_OK;
}

MainGame* MainGame::Create()
{
    MainGame* pInstance = new MainGame;

    if (FAILED(pInstance->Ready_MainGame()))
        Safe_Delete(pInstance);

    return pInstance;
}

void MainGame::Release()
{
    ReleaseDC(g_hWnd, m_hDC);
	Safe_Delete(m_pChessPawn);

	m_pKey_Manager->DestroyInstance();
	m_pServer_Manager->DestroyInstance();
}
