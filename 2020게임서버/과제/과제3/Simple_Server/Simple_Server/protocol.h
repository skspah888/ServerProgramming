#pragma once

constexpr int MIN_BUFFER = 1024;
constexpr int MAX_BUFFER = 4096;
constexpr short PORT = 3500;

constexpr int MAX_USER = 10;


struct INFO
{
	float X;
	float Y;
};

struct OVEREX
{
	OVERLAPPED over;
	WSABUF wsabuf;
};

struct PlayerInfo
{
	SOCKET socket;		// 클라이언트 소켓
	OVEREX overEX;		// overlapped 구조체

	INFO	Info;		// 좌표
	bool	isUsed;		// 사용중 확인
	int		ClientID;	// 클라이언트 아이디
};

struct SEND_DATA
{
	int		ClientID;			// 클라이언트 아이디
	bool	isUsed[MAX_USER] = { false , };	// 플레이중인 유저 판별
	INFO	Info[MAX_USER];		// 클라이언트 좌표 (전체)
};

struct RECV_DATA
{
	DWORD	KeyInput;			// 받은 데이터
};


constexpr float sizeX = (800 / 8);
constexpr float sizeY = (600 / 8);