#include <iostream>
#include <WS2tcpip.h>				// 윈도우 소켓 사용
#include "protocol.h"
using namespace std;

#pragma comment(lib, "Ws2_32.lib")	

// 키입력 확인용
const DWORD KEY_UP = 0x00000001;
const DWORD KEY_DOWN = 0x00000002;
const DWORD KEY_LEFT = 0x00000004;
const DWORD KEY_RIGHT = 0x00000008;
const DWORD KEY_ESCAPE = 0x00000010;

PlayerInfo	Clients[MAX_USER];
int			ClientID = -1;
SEND_DATA	SendData;
RECV_DATA	RecvData;
bool		CheckClient[MAX_USER];

SOCKET		client_socket;

void error_display(const char* msg, int err_no)
{
	WCHAR* lpMsgBuf;
	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, err_no, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPWSTR)&lpMsgBuf, 0, NULL);
	cout << msg;
	wcout << L"  에러 => " << lpMsgBuf << endl;
	while (true);
	LocalFree(lpMsgBuf);
}

int GetID()
{
	ClientID++;
	return ClientID;
}

void Add_New_Client(SOCKET clientSocket);
void CALLBACK recv_complete(DWORD err, DWORD bytes, LPWSAOVERLAPPED over, DWORD flag);
void CALLBACK send_complete(DWORD err, DWORD bytes, LPWSAOVERLAPPED over, DWORD flag);

int main()
{
	wcout.imbue(std::locale("korean"));
	WSADATA WSAData;
	WSAStartup(MAKEWORD(2, 0), &WSAData);

	SOCKET serverSocket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED);
	SOCKADDR_IN serverAddr;		// 서버 주소 선언
	memset(&serverAddr, 0, sizeof(serverAddr));	// 반드시 모두 0으로 초기화 해줘야한다. 
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);
	serverAddr.sin_addr.s_addr = INADDR_ANY;
	::bind(serverSocket, (sockaddr*)&serverAddr, sizeof(serverAddr));
	listen(serverSocket, SOMAXCONN);

	SOCKADDR_IN clientAddr;

	while (true)
	{
		int addr_size = sizeof(clientAddr);
		if(ClientID < MAX_USER)
			client_socket = accept(serverSocket, (sockaddr*)&clientAddr, &addr_size);

		if (SOCKET_ERROR == client_socket)
		{
			error_display("WSAAccept", WSAGetLastError());
		}

		//// 새로운 소켓 추가
		//if (ClientID < MAX_USER)
		//{
		//	std::cout << "Max User Limit exceeded" << endl;
		//	closesocket(client_socket);
		//	continue;
		//}

		int ID = GetID();
		std::cout << "New " << ID << " client accepted" << endl;

		// 클라이언트 정보 셋팅
		Clients[ID].socket = client_socket;
		Clients[ID].ClientID = ID;
		Clients[ID].isUsed = true;
		Clients[ID].Info.X = sizeX / 2;
		Clients[ID].Info.Y = sizeY / 2;
		ZeroMemory(&Clients[ID].overEX.over, sizeof(OVERLAPPED));
		Clients[ID].overEX.over.hEvent = (HANDLE)ID;


		// 서버에 저장된 데이터 업데이트
		SendData.ClientID = ID;
		for (int i = 0; i < MAX_USER; ++i)
		{
			SendData.isUsed[i] = Clients[i].isUsed;
			if (Clients[i].isUsed)
				SendData.Info[i] = Clients[i].Info;
		}

		// WSABUF에 서버에 저장된 클라이언트 전체 정보를 저장 후 전송
		Clients[ID].overEX.wsabuf.buf = reinterpret_cast<char*>(&SendData);
		Clients[ID].overEX.wsabuf.len = sizeof(SendData);

		DWORD flag = 0;
		OVERLAPPED* SendOver = new OVERLAPPED;

		ZeroMemory(SendOver, sizeof(OVERLAPPED));
		SendOver->hEvent = HANDLE(ID);

		int ret = WSASend(client_socket, &Clients[ID].overEX.wsabuf, 1, NULL, flag, SendOver, send_complete);

	}

	closesocket(serverSocket);
	WSACleanup();
}

void Add_New_Client(SOCKET clientSocket)
{
	if (ClientID >= MAX_USER)
	{
		std::cout << "Max User Limit exceeded" << endl;
		closesocket(clientSocket);
		return;
	}

	int ID = ClientID++;
	std::cout << "New " << ID << " client accepted" << endl;

	// 클라이언트 정보 셋팅
	Clients[ID].socket = clientSocket;
	Clients[ID].ClientID = ID;
	Clients[ID].isUsed = true;
	Clients[ID].Info.X = sizeX / 2;
	Clients[ID].Info.Y = sizeY / 2;
	ZeroMemory(&Clients[ID].overEX.over, sizeof(OVERLAPPED));
	Clients[ID].overEX.over.hEvent = (HANDLE)ID;


	// 서버에 저장된 데이터 업데이트
	SendData.ClientID = ID;
	for (int i = 0; i < MAX_USER; ++i)
	{
		SendData.isUsed[i] = Clients[i].isUsed;
		if (Clients[i].isUsed)
			SendData.Info[i] = Clients[i].Info;
	}

	// WSABUF에 서버에 저장된 클라이언트 전체 정보를 저장 후 전송
	Clients[ID].overEX.wsabuf.buf = reinterpret_cast<char*>(&SendData);
	Clients[ID].overEX.wsabuf.len = sizeof(SendData);

	DWORD flag = 0;
	OVERLAPPED* RecvOver = new OVERLAPPED;

	ZeroMemory(RecvOver, sizeof(OVERLAPPED));
	RecvOver->hEvent = HANDLE(ID);

	int ret = WSASend(clientSocket, &Clients[ID].overEX.wsabuf, 1, NULL, flag, RecvOver, send_complete);
}

void CALLBACK send_complete(DWORD err, DWORD bytes, LPWSAOVERLAPPED over, DWORD flag)
{
	// hEvent에 클라이언트의 아이디를 저장했었음
	int ID = reinterpret_cast<int>(over->hEvent);

	if (bytes > 0) 
	{
		delete over;
		ZeroMemory(&Clients[ID].overEX.over, sizeof(OVERLAPPED));
		Clients[ID].overEX.over.hEvent = (HANDLE)ID;
		Clients[ID].overEX.wsabuf.buf = reinterpret_cast<char*>(&RecvData);
		Clients[ID].overEX.wsabuf.len = sizeof(RecvData);

		flag = 0;

		if (CheckClient[ID] == FALSE)
		{
			int ret = WSARecv(Clients[ID].socket, &(Clients[ID].overEX.wsabuf), 1, NULL, &flag, &(Clients[ID].overEX.over), recv_complete);
			CheckClient[ID] = TRUE;
		}
	}
	else
	{
		// 클라이언트 접속 종료
		Clients[ID].isUsed = false;
		Clients[ID].Info.X = 0;
		Clients[ID].Info.Y = 0;
		SendData.isUsed[ID] = false;
		SendData.Info[ID].X= 0;
		SendData.Info[ID].Y = 0;

		cout << ID + 1 << " Client Disconnect!!" << endl;
		
		closesocket(Clients[ID].socket);
		return;
	}

}

void CALLBACK recv_complete(DWORD err, DWORD bytes, LPWSAOVERLAPPED over, DWORD flag)
{
	// hEvent에 클라이언트의 아이디를 저장했었음
	int ID = reinterpret_cast<int>(over->hEvent);

	if (bytes > 0) {
		// Clients[ID].overEX.wsabuf.buf에 저장되어있는 데이터를 기반으로 값 변경
		// 예외처리도 여기서 하기!!

		if (memcmp(&KEY_UP, Clients[ID].overEX.wsabuf.buf, strlen(Clients[ID].overEX.wsabuf.buf)) == 0)
		{
			cout << "Client " << ID << " : 위로 이동" << endl;

			if (Clients[ID].Info.Y + (LONG)-sizeY > 0)
				Clients[ID].Info.Y += (LONG)-sizeY;
		}

		if (memcmp(&KEY_DOWN, Clients[ID].overEX.wsabuf.buf, strlen(Clients[ID].overEX.wsabuf.buf)) == 0)
		{
			cout << "Client " << ID << " : 아래로 이동" << endl;

			if (Clients[ID].Info.Y + (LONG)sizeY < 600)
				Clients[ID].Info.Y += (LONG)sizeY;
		}

		if (memcmp(&KEY_LEFT, Clients[ID].overEX.wsabuf.buf, strlen(Clients[ID].overEX.wsabuf.buf)) == 0)
		{
			cout << "Client " << ID << " : 왼쪽으로 이동" << endl;

			if (Clients[ID].Info.X + (LONG)-sizeX > 0)
				Clients[ID].Info.X += (LONG)-sizeX;
		}

		if (memcmp(&KEY_RIGHT, Clients[ID].overEX.wsabuf.buf, strlen(Clients[ID].overEX.wsabuf.buf)) == 0)
		{
			if (Clients[ID].Info.X + (LONG)sizeX < 800)
			{
				cout << "Client " << ID << " : 오른쪽으로 이동 => " << Clients[ID].Info.X << endl;
				Clients[ID].Info.X += (LONG)sizeX;
			}
		}

		if (memcmp(&KEY_ESCAPE, Clients[ID].overEX.wsabuf.buf, strlen(Clients[ID].overEX.wsabuf.buf)) == 0)
		{
			cout << "Client " << ID << " : 종료" << endl;

			Clients[ID].isUsed = false;
			Clients[ID].Info.X = 0;
			Clients[ID].Info.Y = 0;
			SendData.isUsed[ID] = false;
			SendData.Info[ID].X = 0;
			SendData.Info[ID].Y = 0;

			cout << ID + 1 << " Client Disconnect!!" << endl;

			closesocket(Clients[ID].socket);
			return;
		}

	}
	else
	{
		// 클라이언트 접속 종료
		Clients[ID].isUsed = false;
		Clients[ID].Info.X = 0;
		Clients[ID].Info.Y = 0;
		SendData.isUsed[ID] = false;
		SendData.Info[ID].X = 0;
		SendData.Info[ID].Y = 0;

		cout << ID + 1 << " Client Disconnect!!" << endl;

		closesocket(Clients[ID].socket);
		return;
	}

	// 데이터 전송

	SendData.ClientID = ID;
	for (int i = 0; i < MAX_USER; ++i)
	{
		SendData.isUsed[i] = Clients[i].isUsed;
		if (Clients[i].isUsed)
			SendData.Info[i] = Clients[i].Info;
	}

	Clients[ID].overEX.wsabuf.buf = reinterpret_cast<char*>(&SendData);
	Clients[ID].overEX.wsabuf.len = sizeof(SendData);

	flag = 0;

	OVERLAPPED* SendOver = new OVERLAPPED;
	ZeroMemory(SendOver, sizeof(OVERLAPPED));
	SendOver->hEvent = (HANDLE)ID;
	int retval = WSASend(Clients[ID].socket, &(Clients[ID].overEX.wsabuf), 1, NULL, flag, SendOver, send_complete);

	CheckClient[ID] = false;
}
