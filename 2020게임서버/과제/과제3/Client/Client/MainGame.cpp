#include "framework.h"
#include "MainGame.h"
#include "ChessPawn.h"

MainGame::MainGame()
{
}

MainGame::~MainGame()
{
    Release();
}

DWORD __stdcall MainGame::RecvThread(LPVOID arg)
{
	////while ((ret = WSARecv(Server_Manager::GetInstance()->serverSocket, &Server_Manager::GetInstance()->wsabuf, 1, &num_recv, &flag, NULL, NULL)) != 0)
	//SOCKET sock = Server_Manager::GetInstance()->serverSocket;

	//while (true)
	//{
	//	char messageBuffer[BUF_SIZE];						// NULL문자 까지 더해줌
	//	WSAOVERLAPPED wsaOverlapped;
	//	WSABUF wsabuf;
	//	ZeroMemory(&wsaOverlapped, sizeof(wsaOverlapped));
	//	wsabuf.buf = messageBuffer;

	//	// 받기
	//	DWORD flag = 0;
	//	wsabuf.len = BUF_SIZE;		// 얼만큼 받을지 모르기 때문에 최대사이즈 지정
	//	
	//	int ret = WSARecv(sock, &wsabuf, 1, NULL, &flag, &wsaOverlapped, NULL);
	//	memcpy(&Server_Manager::GetInstance()->playerMove, wsabuf.buf, sizeof(INFO));

	//	cout << Server_Manager::GetInstance()->playerMove.moveX << endl;

	//	Server_Manager::GetInstance()->playerMove.moveX = 0;
	//	Server_Manager::GetInstance()->playerMove.moveY = 0;

	//	//// 말 이동 : 
	//	//Object_Manager::GetInstance()->MovePlayer();
	//}

	//std::cout << "Recv End" << endl;
	
	return 0;
}

HRESULT MainGame::Ready_MainGame()
{
    m_hDC = GetDC(g_hWnd);
    m_pKey_Manager = Key_Manager::GetInstance();
	m_pServer_Manager = Server_Manager::GetInstance();
	m_pObject_Manager = Object_Manager::GetInstance();

    // 체스판 초기화
    for (int i = 0; i < MAP_SIZEX; ++i)
    {
        for (int j = 0; j < MAP_SIZEY; ++j)
        {
            m_ChessMap[j][i].size.x = (g_iWinCX / MAP_SIZEX);       // 100
            m_ChessMap[j][i].size.y = (g_iWinCY / MAP_SIZEY);       // 75

            m_ChessMap[j][i].center.x = j * (g_iWinCX / MAP_SIZEX) + m_ChessMap[j][i].size.x / 2;
            m_ChessMap[j][i].center.y = i * (g_iWinCY / MAP_SIZEY) + m_ChessMap[j][i].size.y / 2;
        }
    }

	// 서버 초기화
	if (FAILED(m_pServer_Manager->Initialize()))
	{
		return E_FAIL;
	};

	//hThread = CreateThread(NULL, 0, RecvThread, NULL, 0, NULL);
	//CloseHandle(hThread);

	// 체스말 생성
	//m_pObject_Manager->AddObject();

    return S_OK;
}

HRESULT MainGame::Update_MainGame(double TimeDeleta)
{
    m_pKey_Manager->CheckKeyInput();
	if (-1 == m_pObject_Manager->Update_GameObject(TimeDeleta))
		return E_FAIL;
    return S_OK;
}

HRESULT MainGame::Render_MainGame()
{
	Rectangle(m_hDC, 0, 0, g_iWinCX * 2, g_iWinCY * 2);

	/////////////////////////////////////////////////////////////////
	 // 체스판 
	for (int i = 0; i < MAP_SIZEY; ++i)
	{
		for (int j = 0; j < MAP_SIZEX; ++j)
		{
			HPEN hPen = CreatePen(PS_SOLID, 1, RGB(25, 75, 125));
			HPEN hOldPen = (HPEN)SelectObject(m_hDC, hPen);
			HBRUSH hBrush = (HBRUSH)GetStockObject(NULL_BRUSH);
			HBRUSH hOldBrush = (HBRUSH)SelectObject(m_hDC, hBrush);

			Rectangle(m_hDC,
				m_ChessMap[j][i].center.x - (m_ChessMap[j][i].size.x / 2),
				m_ChessMap[j][i].center.y - (m_ChessMap[j][i].size.y / 2),
				m_ChessMap[j][i].center.x + (m_ChessMap[j][i].size.x / 2),
				m_ChessMap[j][i].center.y + (m_ChessMap[j][i].size.y / 2));

			SelectObject(m_hDC, hOldBrush);
			DeleteObject(hBrush);
			SelectObject(m_hDC, hOldPen);
			DeleteObject(hPen);
		}
	}

	m_pObject_Manager->Render_GameObject(m_hDC);

	////////////////////////////////////////////////////////////

    return S_OK;
}

MainGame* MainGame::Create()
{
    MainGame* pInstance = new MainGame;

    if (FAILED(pInstance->Ready_MainGame()))
        Safe_Delete(pInstance);

    return pInstance;
}

void MainGame::Release()
{
    ReleaseDC(g_hWnd, m_hDC);

	m_pKey_Manager->DestroyInstance();
	m_pObject_Manager->DestroyInstance();
	m_pServer_Manager->DestroyInstance();
}
