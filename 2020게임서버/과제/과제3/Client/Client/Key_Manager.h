#pragma once

const DWORD KEY_UP = 0x00000001;
const DWORD KEY_DOWN = 0x00000002;
const DWORD KEY_LEFT = 0x00000004;
const DWORD KEY_RIGHT = 0x00000008;

const DWORD KEY_ESCAPE = 0x00000010;
const DWORD KEY_SPACE = 0x00000020;
const DWORD KEY_LBUTTON = 0x00000040;
const DWORD KEY_RBUTTON = 0x00000080;

const DWORD KEY_R = 0x00000800;

const DWORD KEY_W = 0x00001000;
const DWORD KEY_A = 0x00002000;
const DWORD KEY_S = 0x00004000;
const DWORD KEY_D = 0x00008000;

const DWORD KEY_LCTRL = 0x00200000;

const DWORD	KEY_1 = 0x10000000;
const DWORD	KEY_2 = 0x20000000;
const DWORD	KEY_3 = 0x40000000;
const DWORD	KEY_4 = 0x80000000;


class Key_Manager
{
	DECLARE_SINGLETON(Key_Manager)

private:
	explicit Key_Manager();
	~Key_Manager();

public:
	HRESULT		CheckKeyInput();
	bool		KeyDown(DWORD dwKey);
	bool		KeyUp(DWORD dwKey);
	bool		KeyPressing(DWORD dwKey);
	bool		KeyCombine(DWORD dwFirstKey, DWORD dwSecondKey);

private:
	DWORD		m_dwCurKey;
	DWORD		m_dwKeyDowned;
	DWORD		m_dwKeyPressed;
};

