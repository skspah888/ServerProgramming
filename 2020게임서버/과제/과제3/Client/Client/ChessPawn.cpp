#include "framework.h"
#include "ChessPawn.h"

ChessPawn::ChessPawn()
{
}

ChessPawn::~ChessPawn()
{
    Release();
}

HRESULT ChessPawn::Ready_GameObject()
{
    m_pKey_Manager = Key_Manager::GetInstance();
    m_pServer_Manager = Server_Manager::GetInstance();

    m_tInfo.Info.X = (g_iWinCX / MAP_SIZEX) / 2;
    m_tInfo.Info.Y = (g_iWinCY / MAP_SIZEY) / 2;
	/*m_tInfo.size.x = (g_iWinCX / MAP_SIZEX);
	m_tInfo.size.y = (g_iWinCY / MAP_SIZEY);*/

    return S_OK;
}

int ChessPawn::Update_GameObject(double TimeDelta)
{
    if(FAILED(Key_Input()))
        return -1;


    return 0;
}

int ChessPawn::LateUpdate_GameObject(double TimeDeleta)
{
    return 0;
}

HRESULT ChessPawn::Render_GameObject(HDC hDC)
{
    HPEN hPen = CreatePen(PS_SOLID, 1, RGB(25, 75, 125));
    HPEN hOldPen = (HPEN)SelectObject(hDC, hPen);
    HBRUSH hBrush = CreateSolidBrush(RGB(255, 0, 0));
    HBRUSH hOldBrush = (HBRUSH)SelectObject(hDC, hBrush);

    GameObject::Update_Rect();
    /*Ellipse(hDC, m_tInfo.location.x - m_tInfo.size.x/2, m_tInfo.location.y - m_tInfo.size.y/2,
        m_tInfo.location.x + m_tInfo.size.x/2, m_tInfo.location.y + m_tInfo.size.y/2);*/

    Ellipse(hDC, m_tInfo.Info.X - sizeX / 2, m_tInfo.Info.Y - sizeY / 2,
        m_tInfo.Info.X + sizeX / 2, m_tInfo.Info.Y + sizeY / 2);

    DeleteObject(hPen);
    DeleteObject(hBrush);
	SelectObject(hDC, hOldPen);
	SelectObject(hDC, hOldBrush);
    
    return S_OK;
}

HRESULT ChessPawn::Key_Input()
{
    if (m_pKey_Manager->KeyDown(KEY_UP))
    {
        cout << "위" << endl;
        if(FAILED(m_pServer_Manager->UpdateData(KEY_UP)))
            return E_FAIL;
    }

	if (m_pKey_Manager->KeyDown(KEY_DOWN))
	{
        cout << "아래" << endl;
        if (FAILED(m_pServer_Manager->UpdateData(KEY_DOWN)))
            return E_FAIL;
	}

	if (m_pKey_Manager->KeyDown(KEY_RIGHT))
	{
        cout << "오른쪽" << endl;

        if (FAILED(m_pServer_Manager->UpdateData(KEY_RIGHT)))
            return E_FAIL;

	}

	if (m_pKey_Manager->KeyDown(KEY_LEFT))
	{
        cout << "왼쪽" << endl;

        if (FAILED(m_pServer_Manager->UpdateData(KEY_LEFT)))
            return E_FAIL;
	}

    if (m_pKey_Manager->KeyDown(KEY_ESCAPE))
    {
        if (FAILED(m_pServer_Manager->UpdateData(KEY_ESCAPE)))
            return E_FAIL;

        return E_FAIL;
    }

    return S_OK;
}

ChessPawn* ChessPawn::Create()
{
    ChessPawn* pInstance = new ChessPawn;

	if (FAILED(pInstance->Ready_GameObject()))
		Safe_Delete(pInstance);

	return pInstance;
}

void ChessPawn::Release()
{
}
