#pragma once

constexpr int MIN_BUFFER = 1024;
constexpr int MAX_BUFFER = 4096;
constexpr short PORT = 3500;

constexpr int MAX_USER = 10;

struct INFO
{
	float X;
	float Y;
};

struct PlayerInfo
{
	INFO	Info;		// 좌표
	bool	isUsed;		// 사용중 확인
	int		ClientID;	// 클라이언트 아이디
};

struct OVEREX
{
	OVERLAPPED over;
	WSABUF wsabuf;
};

struct RECV_DATA
{
	int		ClinetID;			// 클라이언트 아이디
	bool	isUsed[MAX_USER] = { false , };	// 플레이중인 유저 판별
	INFO	Info[MAX_USER];		// 클라이언트 좌표 (전체)
};

struct SEND_DATA
{
	DWORD	KeyInput;			
};

constexpr float sizeX = (800 / 8);
constexpr float sizeY = (600 / 8);