#include "framework.h"
#include "Object_Manager.h"

IMPLEMENT_SINGLETON(Object_Manager)

Object_Manager::Object_Manager()
{
	for (int i = 0; i < MAX_USER; ++i)
	{
		m_vecChessPawn.emplace_back(ChessPawn::Create());
		m_vecChessPawn[i]->GetInfo().isUsed = false;
	}
}

Object_Manager::~Object_Manager()
{
	for (auto& player : m_vecChessPawn)
		Safe_Delete(player);
}

void Object_Manager::AddObject(RECV_DATA& tRecv)
{
	/*ChessPawn* pObject = ChessPawn::Create();
	if (pObject == nullptr)
		return;

	pObject->GetInfo().ClientID = tRecv.ClinetID;
	pObject->GetInfo().Info = tRecv.Info[tRecv.ClinetID];
	pObject->GetInfo().isUsed = tRecv.isUsed[tRecv.ClinetID];*/

	/*m_ListChessPawn.emplace_back(pObject);*/

	m_vecChessPawn[tRecv.ClinetID]->GetInfo().ClientID = tRecv.ClinetID;
	m_vecChessPawn[tRecv.ClinetID]->GetInfo().Info = tRecv.Info[tRecv.ClinetID];
	m_vecChessPawn[tRecv.ClinetID]->GetInfo().isUsed = tRecv.isUsed[tRecv.ClinetID];

	/*m_Clients[tRecv.ClinetID].GetInfo().ClientID = tRecv.ClinetID;
	m_Clients[tRecv.ClinetID].GetInfo().Info = tRecv.Info[tRecv.ClinetID];
	m_Clients[tRecv.ClinetID].GetInfo().isUsed = tRecv.isUsed[tRecv.ClinetID];*/
}

void Object_Manager::DeleteObject()
{
	//auto iter = m_ListChessPawn.begin();

	//for (; iter != m_ListChessPawn.end(); )
	//{
	//	if (tInfo.id == (*iter)->GetInfo().id)
	//	{
	//		Safe_Delete(*iter);
	//		iter = m_ListChessPawn.erase(iter);
	//		return;
	//	}
	//	else
	//		++iter;
	//}
}

void Object_Manager::ServerUpdate(RECV_DATA& tRecv)
{
	//// 서버에서 건너져온 RecvData를 기반으로 ObjectList를 업데이트 한다.
	//for (int i = 0; i < MAX_USER; ++i)
	//{
	//	m_Clients[i].GetInfo().isUsed = tRecv.isUsed[i];
	//	if (m_Clients[i].GetInfo().isUsed)
	//		m_Clients[i].GetInfo().Info = tRecv.Info[i];
	//}

		// 서버에서 건너져온 RecvData를 기반으로 ObjectList를 업데이트 한다.
	for (int i = 0; i < MAX_USER; ++i)
	{
		m_vecChessPawn[i]->GetInfo().isUsed = tRecv.isUsed[i];
		if (m_vecChessPawn[i]->GetInfo().isUsed)
			m_vecChessPawn[i]->GetInfo().Info = tRecv.Info[i];
	}
}

void Object_Manager::MovePlayer()
{
	/*auto iter = m_ListChessPawn.begin();

	(*iter)->GetInfo().location.x += (LONG)Server_Manager::GetInstance()->GetMoveInfo().moveX;
	(*iter)->GetInfo().location.y += (LONG)Server_Manager::GetInstance()->GetMoveInfo().moveY;

	Server_Manager::GetInstance()->playerMove.moveX = 0;
	Server_Manager::GetInstance()->playerMove.moveY = 0;*/

	//// 예외처리 일단 X
	//if ((*iter)->GetInfo().location.y + (LONG)Server_Manager::GetInstance()->GetMoveInfo().moveY > 0)
	//	(*iter)->GetInfo().location.y += (LONG)Server_Manager::GetInstance()->GetMoveInfo().moveY;

	//if ((*iter)->GetInfo().location.y + (LONG)Server_Manager::GetInstance()->GetMoveInfo().moveY < g_iWinCY)
	//	(*iter)->GetInfo().location.y += (LONG)Server_Manager::GetInstance()->GetMoveInfo().moveY;

	//if ((*iter)->GetInfo().location.x + (LONG)Server_Manager::GetInstance()->GetMoveInfo().moveX < g_iWinCX)
	//	(*iter)->GetInfo().location.x += (LONG)Server_Manager::GetInstance()->GetMoveInfo().moveX;

	//if ((*iter)->GetInfo().location.x + (LONG)Server_Manager::GetInstance()->GetMoveInfo().moveX > 0)
	//	(*iter)->GetInfo().location.x += (LONG)Server_Manager::GetInstance()->GetMoveInfo().moveX;


}

HRESULT Object_Manager::Ready_GameObject()
{
	return S_OK;
}

int Object_Manager::Update_GameObject(double TimeDelta)
{
	for (auto& player : m_vecChessPawn)
	{
		if(player->GetInfo().isUsed)
			player->Update_GameObject(TimeDelta);
	}

	//for (int i = 0; i < MAX_USER; ++i)
	//{
	//	if (m_Clients[i].GetInfo().isUsed)
	//		m_Clients[i].Update_GameObject(TimeDelta);
	//}

	return 0;
}

int Object_Manager::LateUpdate_GameObject(double TimeDeleta)
{
	return 0;
}

HRESULT Object_Manager::Render_GameObject(HDC hDC)
{
	for (auto& player : m_vecChessPawn)
	{
		if (player->GetInfo().isUsed)
			player->Render_GameObject(hDC);
	}

	/*for (int i = 0; i < MAX_USER; ++i)
	{
		if (m_Clients[i].GetInfo().isUsed)
			m_Clients[i].Render_GameObject(hDC);
	}*/
	return S_OK;
}
