#pragma once
class GameObject
{
public:
	explicit GameObject();
	~GameObject();

public:
	PlayerInfo& GetInfo() { return m_tInfo; }
	RECT& GetRect() { return m_tRect; }

public:
	HRESULT		Update_Rect();

public:
	virtual		HRESULT		Ready_GameObject() { return S_OK; }
	virtual		int			Update_GameObject(double TimeDelta) = 0;
	virtual		int			LateUpdate_GameObject(double TimeDeleta) = 0;
	virtual		HRESULT		Render_GameObject(HDC hDC) = 0;

public:
	RECT			m_tRect;
	PlayerInfo		m_tInfo;

public:
	virtual void		Release();

};

