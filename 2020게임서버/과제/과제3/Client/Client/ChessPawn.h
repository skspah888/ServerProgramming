#pragma once
#include "GameObject.h"
class ChessPawn :
    public GameObject
{
public:
    explicit ChessPawn();
    ~ChessPawn();

public:
    // GameObject을(를) 통해 상속됨
    virtual HRESULT Ready_GameObject();
    virtual int Update_GameObject(double TimeDelta) override;
    virtual int LateUpdate_GameObject(double TimeDeleta) override;
    virtual HRESULT Render_GameObject(HDC hDC) override;

public:
    HRESULT      Key_Input();

public:
    static ChessPawn* Create();
    virtual void Release();

private:
    Key_Manager* m_pKey_Manager = nullptr;
    Server_Manager* m_pServer_Manager = nullptr;
};

