#pragma once
class Server_Manager
{
	DECLARE_SINGLETON(Server_Manager)

private:
	explicit Server_Manager();
	~Server_Manager();

public:
	HRESULT			Initialize();						// 클라-서버 초기화
	HRESULT			UpdateData(const DWORD KeyInput);		//

public:
	WSADATA			WSAData;
	SOCKET			serverSocket;		// 서버와 연결하는 소켓
	SOCKADDR_IN		serverAddr;	

	WSABUF			wsabuf;
	WSAOVERLAPPED	wsaOverlapped;

	DWORD num_sent;
	DWORD num_recv;
	DWORD flag;

	SEND_DATA SendData;
	RECV_DATA RecvData;

};

