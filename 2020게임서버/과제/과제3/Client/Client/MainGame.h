#pragma once
class ChessPawn;
class MainGame
{
public:
	explicit MainGame();
	~MainGame();

public:
	static DWORD WINAPI RecvThread(LPVOID arg);
	static void CALLBACK recv_complete(DWORD err, DWORD bytes, LPWSAOVERLAPPED over, DWORD flags);


public:
	HRESULT		Ready_MainGame();
	HRESULT		Update_MainGame(double TimeDeleta);
	HRESULT		Render_MainGame();

private:
	HDC			m_hDC;
	MapInfo		m_ChessMap[MAP_SIZEX][MAP_SIZEY];
	WSABUF				wsaBuf;
	WSAOVERLAPPED		over;

private:
	HANDLE		hThread;

private:
	Key_Manager* m_pKey_Manager = nullptr;
	Server_Manager* m_pServer_Manager = nullptr;
	Object_Manager* m_pObject_Manager = nullptr;


public:
	static MainGame*	Create();
	void				Release();
};

