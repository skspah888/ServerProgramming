#include "framework.h"
#include "GameObject.h"

GameObject::GameObject()
{
	ZeroMemory(&m_tRect, sizeof(RECT));
	ZeroMemory(&m_tInfo, sizeof(INFO));
}

GameObject::~GameObject()
{
}

HRESULT GameObject::Update_Rect()
{
	/*m_tRect.left = LONG(m_tInfo.location.x - (m_tInfo.size.x / 2));
	m_tRect.right = LONG(m_tInfo.location.x + (m_tInfo.size.x / 2));
	m_tRect.top = LONG(m_tInfo.location.y - (m_tInfo.size.y / 2));
	m_tRect.bottom = LONG(m_tInfo.location.y + (m_tInfo.size.y / 2));*/


	m_tRect.left = LONG(m_tInfo.Info.X - (sizeX / 2));
	m_tRect.right = LONG(m_tInfo.Info.X + (sizeX / 2));
	m_tRect.top = LONG(m_tInfo.Info.Y - (sizeY / 2));
	m_tRect.bottom = LONG(m_tInfo.Info.Y + (sizeY / 2));
	return S_OK;
}

void GameObject::Release()
{
}
