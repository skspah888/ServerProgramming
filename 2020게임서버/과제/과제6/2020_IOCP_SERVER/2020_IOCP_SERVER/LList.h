#pragma once
#include <mutex>
using namespace std;

class NODE
{
public:
	int key;
	NODE* next;
	bool marked;
	mutex nlock;

	NODE() { next = nullptr; }
	NODE(int key_value)
	{
		next = nullptr;
		key = key_value;
		marked = false;
	}
	~NODE() {}

	void lock()
	{
		nlock.lock();
	}

	void unlock()
	{
		nlock.unlock();
	}

	NODE& operator=(const NODE& rhs)
	{
		key = rhs.key;
		next = rhs.next;
		marked = rhs.marked;
		return *this;
	}
};

class LLIST
{
public:
	NODE head, tail;
	NODE* freelist;
	NODE freetail;
	mutex fl;

	LLIST();
	~LLIST();

	LLIST& operator=(const LLIST& rhs);

	void Init();
	bool validate(NODE* pred, NODE* curr);
	void recycle_freelist();
	bool Add(int key);
	bool Remove(int key);
	bool Contains(int key);
	bool Empty();
	void Print_Display(int value = 20);
};

