#include "LList.h"
#include <iostream>

using namespace std;

LLIST::LLIST()
{
	head.key = 0x80000000;
	tail.key = 0x7FFFFFFF;
	head.next = &tail;
	freetail.key = 0x7FFFFFFF;
	freelist = &freetail;
}

LLIST::~LLIST()
{
}

LLIST& LLIST::operator=(const LLIST& rhs)
{
	head = rhs.head;
	freelist = rhs.freelist;
	freetail = rhs.freetail;
	return *this;
}

void LLIST::Init()
{
	NODE* ptr;
	while (head.next != &tail) {
		ptr = head.next;
		head.next = head.next->next;
		delete ptr;
	}
}

bool LLIST::validate(NODE* pred, NODE* curr)
{
	return !pred->marked && !curr->marked && pred->next == curr;
}

void LLIST::recycle_freelist()
{
	NODE* p = freelist;
	while (p != &freetail)
	{
		NODE* n = p->next;
		delete p;
		p = n;
	}
	freelist = &freetail;
}

bool LLIST::Add(int key)
{
	NODE* pred, * curr;
	pred = &head;
	curr = pred->next;
	while (curr->key < key) {
		pred = curr;
		curr = curr->next;
	}
	pred->lock();
	curr->lock();

	if (validate(pred, curr))
	{
		if (key == curr->key)
		{
			curr->unlock();
			pred->unlock();
			return false;
		}
		else
		{
			NODE* node = new NODE(key);
			node->next = curr;
			pred->next = node;
			curr->unlock();
			pred->unlock();
			return true;
		}
	}

	curr->unlock();
	pred->unlock();
	return false;
}

bool LLIST::Remove(int key)
{
	NODE* pred, * curr;
	pred = &head;
	curr = pred->next;
	while (curr->key < key)
	{
		pred = curr;
		curr = curr->next;
	}

	pred->lock();
	curr->lock();
	if (validate(pred, curr)) {
		if (key == curr->key)
		{
			curr->marked = true;
			pred->next = curr->next;
			fl.lock();
			curr->next = freelist;
			freelist = curr;
			fl.unlock();
			pred->unlock();
			curr->unlock();
			return true;
		}
		else
		{
			pred->unlock();
			curr->unlock();
			return false;
		}
	}

	curr->unlock();
	pred->unlock();
	return false;
}

bool LLIST::Contains(int key)
{
	NODE* curr;
	curr = &head;
	while (curr->key < key)
	{
		curr = curr->next;
	}

	return curr->key == key && !curr->marked;
}

bool LLIST::Empty()
{
	NODE* p = head.next;
	if (p != &tail)
		return true;

	return false;
}

void LLIST::Print_Display(int value)
{
	int num = value;
	NODE* p = head.next;

	while (p != &tail) {
		cout << p->key << ", ";
		p = p->next;
		num--;
		if (num == 0)
			break;
	}
	cout << endl;
}
