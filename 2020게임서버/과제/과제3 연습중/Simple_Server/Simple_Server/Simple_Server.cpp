#include <iostream>
#include <WS2tcpip.h>				// 윈도우 소켓 사용
#include "protocol.h"
using namespace std;

#pragma comment(lib, "Ws2_32.lib")	

// 키입력 확인용
const DWORD KEY_UP = 0x00000001;
const DWORD KEY_DOWN = 0x00000002;
const DWORD KEY_LEFT = 0x00000004;
const DWORD KEY_RIGHT = 0x00000008;
const DWORD KEY_ESCAPE = 0x00000010;


int CurrentID = 0;
int SendCurrentID = 0;

struct OVER_EX
{
	WSAOVERLAPPED	wsa_over;
	int				type;				// Send인지 Recv인지
	WSABUF			wsa_buf;
	unsigned char	iocp_buf[MIN_BUFFER];
};

void error_display(const char* msg, int err_no)
{
	WCHAR* lpMsgBuf;
	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, err_no, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPWSTR)&lpMsgBuf, 0, NULL);
	cout << msg;
	wcout << L"  에러 => " << lpMsgBuf << endl;
	while (true);
	LocalFree(lpMsgBuf);
}

void CALLBACK recv_complete(DWORD err, DWORD bytes, LPWSAOVERLAPPED over, DWORD flags);

void CALLBACK send_complete(DWORD err, DWORD bytes, LPWSAOVERLAPPED over, DWORD flags)
{
	if (bytes > 0) {
		cout << "클라이언트에 데이터 전송완료" << endl;
	}
	else
	{
		closesocket(players[CurrentID].client_socket);
		return;
	}

	// ret이 -1 이면 에러
	players[SendCurrentID].wsabuf.len = MAX_BUFFER;
	ZeroMemory(over, sizeof(*over));
	int ret = WSARecv(players[SendCurrentID].client_socket, &players[SendCurrentID].wsabuf, 1, NULL, &flags, over, recv_complete);

	//if (SOCKET_ERROR == ret) {
	//	error_display("WSARecv", WSAGetLastError());
	//}
}

// overlapped callback 함수 
void CALLBACK recv_complete(DWORD err, DWORD bytes, LPWSAOVERLAPPED over, DWORD flags)
{
	if (bytes > 0) {
		if (memcmp(&KEY_UP, players[CurrentID].wsabuf.buf, strlen(players[CurrentID].wsabuf.buf)) == 0)
		{
			cout << "클라이언트 "<< CurrentID  <<" : 위로 이동" << endl;

			if (players[CurrentID].Y + (LONG)-sizeY > 0)
				players[CurrentID].Y += (LONG)-sizeY;

		}

		//if (memcmp(&KEY_DOWN, wsabuf.buf, strlen(wsabuf.buf)) == 0)
		if (memcmp(&KEY_DOWN, players[CurrentID].wsabuf.buf, strlen(players[CurrentID].wsabuf.buf)) == 0)
		{
			cout << "클라이언트 " << CurrentID << " : 아래로 이동" << endl;

			if (players[CurrentID].Y + (LONG)sizeY < 600)
				players[CurrentID].Y += (LONG)sizeY;

		}

		//if (memcmp(&KEY_LEFT, wsabuf.buf, strlen(wsabuf.buf)) == 0)
		if (memcmp(&KEY_LEFT, players[CurrentID].wsabuf.buf, strlen(players[CurrentID].wsabuf.buf)) == 0)
		{
			cout << "클라이언트 " << CurrentID << " : 왼쪽으로 이동" << endl;

			if (players[CurrentID].X + (LONG)-sizeX > 0)
				players[CurrentID].X += (LONG)-sizeX;

		}

		//if (memcmp(&KEY_RIGHT, wsabuf.buf, strlen(wsabuf.buf)) == 0)
		if (memcmp(&KEY_RIGHT, players[CurrentID].wsabuf.buf, strlen(players[CurrentID].wsabuf.buf)) == 0)
		{
			cout << "클라이언트 " << CurrentID << " : 오른쪽으로 이동" << endl;

			if (players[CurrentID].X + (LONG)sizeX < 800)
				players[CurrentID].X += (LONG)sizeX;
		}

		//if (memcmp(&KEY_ESCAPE, wsabuf.buf, strlen(wsabuf.buf)) == 0)
		if (memcmp(&KEY_ESCAPE, players[CurrentID].wsabuf.buf, strlen(players[CurrentID].wsabuf.buf)) == 0)
		{
			cout << "클라이언트 " << CurrentID << " : 종료" << endl;
			return;
		}

	}
	else
	{
		closesocket(players[CurrentID].client_socket);
		cout << "Client connection closed\n";
		return;
	}


	memcpy(players[CurrentID].wsabuf.buf, &players, sizeof(players));
	players[CurrentID].wsabuf.len = sizeof(players);
	ZeroMemory(over, sizeof(*over));

	// 전체 클라이언트에게 데이터 전송
	for (int i = 0; i <= CurrentID; ++i)
	{
		SendCurrentID = i;
		int ret = WSASend(players[i].client_socket, &players[i].wsabuf, 1, NULL, NULL, over, send_complete);
	}
}

void Add_Client(SOCKET Clientsock)
{
	int i = 0;
	for (i = 0; i < MAX_USER; ++i)
	{
		if (!players[i].in_use)
			break;
	}

	if (MAX_USER == i)
	{
		cout << "Max user limit exceeded" << endl;
		closesocket(Clientsock);
		return;
	}
	else
	{
		CurrentID = playerNumber;
		players[CurrentID].X = sizeX / 2;
		players[CurrentID].Y = sizeY / 2;
		players[CurrentID].CX = sizeX;
		players[CurrentID].CY = sizeY;
		players[CurrentID].id = ++playerNumber;
		players[CurrentID].in_use = true;
		players[CurrentID].type = EVENT_LOGIN;
		ZeroMemory(&players[CurrentID].overlapped, sizeof(players[CurrentID].overlapped));	// overlapped 초기화(필수);
		players[CurrentID].wsabuf.buf = players[CurrentID].messageBuffer;
		players[CurrentID].wsabuf.len = MAX_BUFFER;


		cout << players[CurrentID].id << " client accepted.\n";

		//// 전체 클라이언트에게 데이터 전송
		//unsigned char* packet = reinterpret_cast<unsigned char*>(&players[CurrentID]);
		//OVER_EX* send_over = new OVER_EX;
		////memcpy(send_over->iocp_buf, packet, packet[0]);
		//memcpy(send_over->iocp_buf, &players[CurrentID], sizeof(players[CurrentID]));
		//send_over->type = EVENT_LOGIN;
		//memcpy(send_over->wsa_buf.buf, send_over->iocp_buf, sizeof(send_over->iocp_buf));
		//send_over->wsa_buf.buf = reinterpret_cast<char*>(send_over->iocp_buf);
		//send_over->wsa_buf.len = MIN_BUFFER;
		//ZeroMemory(&send_over->wsa_over, sizeof(send_over->wsa_over));

		//for (auto& Client : players)
		//{
		//	if(Client.in_use)
		//		int retval = WSASend(Client.client_socket, &send_over->wsa_buf, 1, NULL, 0, &send_over->wsa_over, NULL);
		//}
	}
	
	// 받기
	DWORD flags = 0;
	int ret = WSARecv(players[CurrentID].client_socket, &players[CurrentID].wsabuf, 1, NULL, &flags, &players[CurrentID].overlapped, recv_complete);
}

int main()
{
	wcout.imbue(std::locale("korean"));

	for (auto& client : players)
	{
		client.in_use = false;
	}

	WSADATA WSAData;
	WSAStartup(MAKEWORD(2, 0), &WSAData);	// WSACleanup과 쌍을 이룸, 윈속 사용을 알려주는 것, 무조건 이렇게 한다 그냥

	SOCKET listenSocket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);		// 서버와 연결하는 소켓 생성
	SOCKADDR_IN serverAddr;		// 서버 주소 선언
	memset(&serverAddr, 0, sizeof(SOCKADDR_IN));	// 반드시 모두 0으로 초기화 해줘야한다. 
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(SERVER_PORT);
	serverAddr.sin_addr.s_addr = INADDR_ANY;
	::bind(listenSocket, (sockaddr*)&serverAddr, sizeof(serverAddr));
	listen(listenSocket, 5);

	SOCKADDR_IN client_addr;	

	while (true)
	{
		int addr_size = sizeof(client_addr);
		players[playerNumber].client_socket = accept(listenSocket, (sockaddr*)&client_addr, &addr_size);
		if (SOCKET_ERROR == players[playerNumber].client_socket)
		{
			error_display("WSAAccept", WSAGetLastError());
		}
		
		// 클라이언트 객체 추가
		Add_Client(players[playerNumber].client_socket);
	}

	closesocket(listenSocket);
	WSACleanup();
}