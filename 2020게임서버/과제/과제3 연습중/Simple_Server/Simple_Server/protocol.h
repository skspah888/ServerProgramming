#pragma once

constexpr int MAX_BUFFER = 4096;
constexpr int MIN_BUFFER = 1024;
constexpr short SERVER_PORT = 3500;

constexpr int MAX_ID_LEN = 10;
constexpr int MAX_USER = 10;		// 최대 동접

constexpr int EVENT_LOGIN = 1;
constexpr int EVENT_MOVE = 2;
constexpr int EVENT_LEAVE = 3;
constexpr int EVENT_EXIT = 4;


struct playerInfo
{
	int		type;
	bool	in_use;				// 현재 사용중인지 아닌지 판별
	int		id;
	float	X;
	float	Y;
	float	CX;
	float	CY;

	// Callback 함수가 알기 위해 전역변수로 선언
	char messageBuffer[MIN_BUFFER];
	WSABUF wsabuf;
	SOCKET client_socket;
	WSAOVERLAPPED overlapped;
};

//INFO playerMove;
playerInfo		players[MAX_USER];
int				playerNumber = 0;

constexpr float sizeX = (800 / 8);
constexpr float sizeY = (600 / 8);
