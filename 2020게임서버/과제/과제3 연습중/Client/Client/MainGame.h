#pragma once

#include "ChessPawn.h"

class ChessPawn;
class MainGame
{
public:
	explicit MainGame();
	~MainGame();

public:
	static DWORD WINAPI RecvThread(LPVOID arg);
	static DWORD WINAPI RecvThread2(LPVOID arg);
	int	recvn(SOCKET s, char* buf, int len, int flags);


public:
	HRESULT		Ready_MainGame();
	HRESULT		Update_MainGame(double TimeDeleta);
	HRESULT		Render_MainGame();

private:
	HDC			m_hDC;
	MapInfo		m_ChessMap[MAP_SIZEX][MAP_SIZEY];

private:
	HANDLE		hThread;


private:
	Key_Manager* m_pKey_Manager = nullptr;
	Server_Manager* m_pServer_Manager = nullptr;
	Object_Manager* m_pObject_Manager = nullptr;


public:
	static MainGame*	Create();
	void				Release();
};

