#pragma once

#include "protocol.h"

class Server_Manager
{
	DECLARE_SINGLETON(Server_Manager)

private:
	explicit Server_Manager();
	~Server_Manager();

public:
	WSADATA			WSAData;
	SOCKET			serverSocket;		// 서버와 연결하는 소켓
	SOCKADDR_IN		serverAddr;

	WSAOVERLAPPED wsaOverlapped;

public:
	HRESULT			Initialize();		// 클라-서버 초기화
	HRESULT			Update();			// 서버 업데이트 [ 서버에 데이터 전송 ]
	HRESULT			ReceiveData(playerInfo& playerinfo, const DWORD KeyInput);		// 서버에 데이터 전송
	HRESULT			SendData(playerInfo& playerinfo, const DWORD KeyInput);			// 서버에 데이터 전송

};

