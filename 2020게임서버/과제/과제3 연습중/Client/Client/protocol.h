#pragma once

constexpr int MAX_BUFFER = 4096;
constexpr int MIN_BUFFER = 1024;
constexpr short SERVER_PORT = 3500;

constexpr int EVENT_LOGIN = 1;
constexpr int EVENT_MOVE = 2;
constexpr int EVENT_LEAVE = 3;
constexpr int EVENT_EXIT = 4;

struct playerInfo
{
	int		type;
	bool	in_use;				// 현재 사용중인지 아닌지 판별
	int		id;
	float	X;
	float	Y;
	float	CX;
	float	CY;

	// Callback 함수가 알기 위해 전역변수로 선언
	char messageBuffer[MIN_BUFFER +1];
	WSABUF wsabuf;
	SOCKET client_socket;
	WSAOVERLAPPED overlapped;
};


// 소켓 정보 저장을 위한 구조체와 변수
struct SOCKETINFO
{
	WSAOVERLAPPED overlapped;
	SOCKET sock;
	char buf[MIN_BUFFER + 1];
	int recvbytes;
	int sendbytes;
	WSABUF wsabuf;
	int EventType;
};