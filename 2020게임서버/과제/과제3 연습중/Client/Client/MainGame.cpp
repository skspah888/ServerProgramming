#include "framework.h"
#include "MainGame.h"
//#include "ChessPawn.h"

MainGame::MainGame()
{
}

MainGame::~MainGame()
{
    Release();
}

DWORD __stdcall MainGame::RecvThread(LPVOID arg)
{
	// 데이터 통신에 사용할 변수
	int ret = 0;

	// 받기 1. int size
	WSABUF RecvBUF;
	DWORD num_recv;
	DWORD flag = 0;
	char msgBuf[1024];
	RecvBUF.buf = msgBuf;
	RecvBUF.len = MIN_BUFFER;			// 얼만큼 받을지 모르기 때문에 최대사이즈 지정

	cout << "RecvThread" << endl;

	//
	SOCKETINFO ptr;
	ptr.recvbytes = 0;

	// 데이터 받기
	ZeroMemory(&ptr.overlapped, sizeof(ptr.overlapped));
	ptr.overlapped.hEvent = 0;
	ptr.wsabuf.buf = msgBuf;
	ptr.wsabuf.len = MIN_BUFFER;

	while (true)
	{
//		ret = WSARecv(Server_Manager::GetInstance()->serverSocket, &ptr.wsabuf, 1, &num_recv, &flag, &ptr.overlapped, NULL);
		ret = WSARecv(Server_Manager::GetInstance()->serverSocket, &RecvBUF, 1, &num_recv, &flag, NULL, NULL);
		Sleep(100);

		cout << "Data Recv" << endl;

		if (ret == -1)
		{
			cout << "Recv Error" << endl;
			return -1;
		}
		SOCKETINFO otherInfo;
		memcpy(&otherInfo, RecvBUF.buf, sizeof(otherInfo));

		int i = 0;

		/*playerInfo otherInfo;
		memcpy(&otherInfo, RecvBUF.buf, sizeof(otherInfo));
		cout << "ClientID : " << otherInfo.id << endl;*/


		
		// 로그인 :  m_pObject_Manager->AddObject();

		// 말 이동 : 
	}

	cout << "Recv End" << endl;

	return 0;
}

int MainGame::recvn(SOCKET s, char* buf, int len, int flags)
{
	int received;
	char* ptr = buf;
	int left = len;

	while (left > 0) {
		received = recv(s, ptr, left, flags);
		if (received == SOCKET_ERROR)
			return SOCKET_ERROR;
		else if (received == 0)
			break;
		left -= received;
		ptr += received;
	}

	return (len - left);
}

HRESULT MainGame::Ready_MainGame()
{
    m_hDC = GetDC(g_hWnd);
    m_pKey_Manager = Key_Manager::GetInstance();
	m_pServer_Manager = Server_Manager::GetInstance();
	m_pObject_Manager = Object_Manager::GetInstance();

    // 체스판 초기화
    for (int i = 0; i < MAP_SIZEX; ++i)
    {
        for (int j = 0; j < MAP_SIZEY; ++j)
        {
            m_ChessMap[j][i].size.x = (g_iWinCX / MAP_SIZEX);       // 100
            m_ChessMap[j][i].size.y = (g_iWinCY / MAP_SIZEY);       // 75

            m_ChessMap[j][i].center.x = j * (g_iWinCX / MAP_SIZEX) + m_ChessMap[j][i].size.x / 2;
            m_ChessMap[j][i].center.y = i * (g_iWinCY / MAP_SIZEY) + m_ChessMap[j][i].size.y / 2;
        }
    }

	// 서버 초기화
	if (FAILED(m_pServer_Manager->Initialize()))
	{
		return E_FAIL;
	};

	hThread = CreateThread(NULL, 0, RecvThread, NULL, 0, NULL);

	// 서버 연결 완료
	char messageBuffer[1000] = "접속 완료";
	WSABUF wsaBuf;
	wsaBuf.buf = messageBuffer;
	wsaBuf.len = MIN_BUFFER;

	// 보내기
	DWORD num_sent;	// 전송된 길이 반환용도
	WSASend(m_pServer_Manager->serverSocket, &wsaBuf, 1, &num_sent, 0, NULL, NULL);		// 버퍼 1개만 보내니까 1


	// 체스말 생성
	//m_pChessPawn = ChessPawn::Create();
	m_pObject_Manager->AddObject();

	
    return S_OK;
}

HRESULT MainGame::Update_MainGame(double TimeDeleta)
{
    m_pKey_Manager->CheckKeyInput();

	/*if (-1 == m_pChessPawn->Update_GameObject(TimeDeleta))
		return E_FAIL;*/

	if (-1 == m_pObject_Manager->Update_GameObject(TimeDeleta))
		return E_FAIL;

    return S_OK;
}

HRESULT MainGame::Render_MainGame()
{
	Rectangle(m_hDC, 0, 0, g_iWinCX * 2, g_iWinCY * 2);

	/////////////////////////////////////////////////////////////////
	 // 체스판 
	for (int i = 0; i < MAP_SIZEY; ++i)
	{
		for (int j = 0; j < MAP_SIZEX; ++j)
		{
			HPEN hPen = CreatePen(PS_SOLID, 1, RGB(25, 75, 125));
			HPEN hOldPen = (HPEN)SelectObject(m_hDC, hPen);
			HBRUSH hBrush = (HBRUSH)GetStockObject(NULL_BRUSH);
			HBRUSH hOldBrush = (HBRUSH)SelectObject(m_hDC, hBrush);

			Rectangle(m_hDC,
				m_ChessMap[j][i].center.x - (m_ChessMap[j][i].size.x / 2),
				m_ChessMap[j][i].center.y - (m_ChessMap[j][i].size.y / 2),
				m_ChessMap[j][i].center.x + (m_ChessMap[j][i].size.x / 2),
				m_ChessMap[j][i].center.y + (m_ChessMap[j][i].size.y / 2));

			SelectObject(m_hDC, hOldBrush);
			DeleteObject(hBrush);
			SelectObject(m_hDC, hOldPen);
			DeleteObject(hPen);
		}
	}
	
	m_pObject_Manager->Render_GameObject(m_hDC);

    return S_OK;
}

MainGame* MainGame::Create()
{
    MainGame* pInstance = new MainGame;

    if (FAILED(pInstance->Ready_MainGame()))
        Safe_Delete(pInstance);

    return pInstance;
}

void MainGame::Release()
{
    ReleaseDC(g_hWnd, m_hDC);

	CloseHandle(hThread);

	m_pKey_Manager->DestroyInstance();
	m_pObject_Manager->DestroyInstance();
	m_pServer_Manager->DestroyInstance();
}
