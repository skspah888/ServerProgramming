#pragma once

#include "ChessPawn.h"

class Object_Manager
{
	DECLARE_SINGLETON(Object_Manager)

private:
	explicit Object_Manager();
	~Object_Manager();

public:
	void				AddObject();
	void				DeleteObject(playerInfo& tInfo);

public:
	// GameObject을(를) 통해 상속됨
	HRESULT Ready_GameObject();
	int Update_GameObject(double TimeDelta);
	int LateUpdate_GameObject(double TimeDeleta);
	HRESULT Render_GameObject(HDC hDC);

public:
	list<ChessPawn*>	m_ListChessPawn;
};

