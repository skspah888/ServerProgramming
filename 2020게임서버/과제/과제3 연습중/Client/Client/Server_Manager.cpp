#include "framework.h"
#include "Server_Manager.h"

IMPLEMENT_SINGLETON(Server_Manager)

Server_Manager::Server_Manager()
{
}

Server_Manager::~Server_Manager()
{
	closesocket(serverSocket);
	WSACleanup();
}

HRESULT Server_Manager::Initialize()
{
	WSADATA WSAData;
	WSAStartup(MAKEWORD(2, 0), &WSAData);	// WSACleanup과 쌍을 이룸, 윈속 사용을 알려주는 것, 무조건 이렇게 한다 그냥
	serverSocket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);		// 서버와 연결하는 소켓 생성
	serverAddr;		// 서버 주소 선언
	memset(&serverAddr, 0, sizeof(SOCKADDR_IN));	// 반드시 모두 0으로 초기화 해줘야한다. 
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);

	char SERVER_ADDR[50];
	cout << "IP 주소 입력 [0 입력시 자기자신 주소 사용] : ";
	cin.getline(SERVER_ADDR, 50);		// 한줄 몽땅 입력을 받기위해 getline 사용

	if (SERVER_ADDR[0] == '0')
		strcpy(SERVER_ADDR, "127.0.0.1");

	inet_pton(AF_INET, SERVER_ADDR, &serverAddr.sin_addr);	// 문자 주소를 번역하여 집어 넣음
	WSAConnect(serverSocket, (sockaddr*)&serverAddr, sizeof(serverAddr), NULL, NULL, NULL, NULL);

	cout << "연결한 서버 IP 주소 : " << SERVER_ADDR << endl;
	cout << "ESC 입력시 종료됩니다." << endl;
	cout << "ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ" << endl;


	///////////// 로그인 정보 받기
	//WSABUF wsabuf;

	//DWORD num_recv;
	//DWORD flag = 0;
	//wsabuf.len = BUF_SIZE;		// 얼만큼 받을지 모르기 때문에 최대사이즈 지정
	//WSARecv(serverSocket, &wsabuf, 1, &num_recv, &flag, NULL, NULL);
	//cout << "Received " << num_recv << " Bytes [" << wsabuf.buf << "]\n";

	return S_OK;
}

HRESULT Server_Manager::Update()
{
	char messageBuffer[BUF_SIZE + 1];			// NULL문자 까지 더해줌
	cout << "Enter message: ";
	cin.getline(messageBuffer, BUF_SIZE);		// 한줄 몽땅 입력을 받기위해 getline 사용


	WSABUF wsabuf;
	wsabuf.buf = messageBuffer;
	wsabuf.len = static_cast<ULONG>(strlen(messageBuffer) + 1);		// 버퍼 길이 + NULL

	if (1 == wsabuf.len)		// 데이터가 하나도 없으면 빠져나가게 함
		return S_OK;

	// 보내기
	DWORD num_sent;	// 전송된 길이 반환용도
	WSASend(serverSocket, &wsabuf, 1, &num_sent, 0, NULL, NULL);		// 버퍼 1개만 보내니까 1
	cout << "Sent " << wsabuf.len << " Bytes [" << messageBuffer << "]\n";

	// 받기
	DWORD num_recv;
	DWORD flag = 0;
	wsabuf.len = BUF_SIZE;		// 얼만큼 받을지 모르기 때문에 최대사이즈 지정
	WSARecv(serverSocket, &wsabuf, 1, &num_recv, &flag, NULL, NULL);
	cout << "Received " << num_recv << " Bytes [" << wsabuf.buf << "]\n";


	return S_OK;
}

HRESULT Server_Manager::ReceiveData(playerInfo& playerinfo, const DWORD KeyInput)
{
	playerinfo.wsabuf.buf = playerinfo.messageBuffer;
	playerinfo.wsabuf.len = MAX_BUFFER;		// 버퍼 길이 + NULL

	memcpy(playerinfo.messageBuffer, &KeyInput, MAX_BUFFER+1);


	//if (1 == playerinfo.wsabuf.len)		// 데이터가 하나도 없으면 빠져나가게 함
	//	return S_OK;

	// 보내기
	DWORD num_sent;	// 전송된 길이 반환용도
	WSASend(serverSocket, &playerinfo.wsabuf, 1, &num_sent, 0, NULL, NULL);		// 버퍼 1개만 보내니까 1
	//cout << "Sent " << wsabuf.len << " Bytes [" << messageBuffer << "]\n";



	// 받기 1. int size
	WSABUF RecvBUF;
	DWORD num_recv;
	DWORD flag = 0;
	RecvBUF.len = BUF_SIZE;												// 얼만큼 받을지 모르기 때문에 최대사이즈 지정
	WSARecv(serverSocket, &RecvBUF, 1, &num_recv, &flag, NULL, NULL);

	playerInfo otherInfo;

	memcpy(&otherInfo, RecvBUF.buf, sizeof(RecvBUF.buf));




	//cout << "Received " << num_recv << " Bytes [" << wsabuf.buf << "]\n";

	return S_OK;
}

HRESULT Server_Manager::SendData(playerInfo& playerinfo, const DWORD KeyInput)
{
	cout << "SendData" << endl;

	memcpy(playerinfo.messageBuffer, &KeyInput, sizeof(KeyInput));

	ZeroMemory(&wsaOverlapped, sizeof(wsaOverlapped));
	playerinfo.wsabuf.buf = playerinfo.messageBuffer;
	playerinfo.wsabuf.len = static_cast<int>(strlen(playerinfo.messageBuffer));		// 버퍼 길이 + NULL


	// 보내기
	DWORD num_sent;	// 전송된 길이 반환용도
	WSASend(serverSocket, &playerinfo.wsabuf, 1, &num_sent, 0, &wsaOverlapped, NULL);		// 버퍼 1개만 보내니까 1
	//cout << "Sent " << wsabuf.len << " Bytes [" << messageBuffer << "]\n";

	return S_OK;
}
