#include "framework.h"
#include "GameObject.h"

GameObject::GameObject()
{
	ZeroMemory(&m_tRect, sizeof(RECT));
	ZeroMemory(&m_tInfo, sizeof(playerInfo));
}

GameObject::~GameObject()
{
}

HRESULT GameObject::Update_Rect()
{
	m_tRect.left = LONG(m_tInfo.X - (m_tInfo.CX / 2));
	m_tRect.right = LONG(m_tInfo.X + (m_tInfo.CX / 2));
	m_tRect.top = LONG(m_tInfo.Y - (m_tInfo.CY / 2));
	m_tRect.bottom = LONG(m_tInfo.Y + (m_tInfo.CY / 2));
	return S_OK;
}

void GameObject::Release()
{
}
