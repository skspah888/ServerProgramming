#pragma once

const unsigned int g_iWinCX = 800;
const unsigned int g_iWinCY = 600;

const unsigned int g_iBufferX = 1000;
const unsigned int g_iBufferY = 1000;

const unsigned int MAP_SIZEX = 8;
const unsigned int MAP_SIZEY = 8;

extern HINSTANCE g_hInst;
extern HWND g_hWnd;