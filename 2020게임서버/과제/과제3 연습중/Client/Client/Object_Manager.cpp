#include "framework.h"
#include "Object_Manager.h"

IMPLEMENT_SINGLETON(Object_Manager)

Object_Manager::Object_Manager()
{
}

Object_Manager::~Object_Manager()
{
	for (auto& player : m_ListChessPawn)
		Safe_Delete(player);
}

void Object_Manager::AddObject()
{
	ChessPawn* pObject = ChessPawn::Create();
	if (pObject == nullptr)
		return;

	m_ListChessPawn.emplace_back(pObject);
}

void Object_Manager::DeleteObject(playerInfo& tInfo)
{
	auto iter = m_ListChessPawn.begin();

	for (; iter != m_ListChessPawn.end(); )
	{
		if (tInfo.id == (*iter)->GetInfo().id)
		{
			Safe_Delete(*iter);
			iter = m_ListChessPawn.erase(iter);
			return;
		}
		else
			++iter;
	}
}

HRESULT Object_Manager::Ready_GameObject()
{
	return S_OK;
}

int Object_Manager::Update_GameObject(double TimeDelta)
{
	for (auto& player : m_ListChessPawn)
		player->Update_GameObject(TimeDelta);

	return 0;
}

int Object_Manager::LateUpdate_GameObject(double TimeDeleta)
{
	return 0;
}

HRESULT Object_Manager::Render_GameObject(HDC hDC)
{
	for (auto& player : m_ListChessPawn)
		player->Render_GameObject(hDC);
	return S_OK;
}
