#pragma once

constexpr int MAX_BUFFER = 4096;
constexpr int MIN_BUFFER = 1024;
constexpr short SERVER_PORT = 3500;

constexpr int MAX_ID_LEN = 10;
constexpr int MAX_USER = 10;		// 최대 동접

constexpr int EVENT_LOGIN = 1;
constexpr int EVENT_MOVE = 2;
constexpr int EVENT_LEAVE = 3;
constexpr int EVENT_EXIT = 4;


// 소켓 정보 저장을 위한 구조체와 변수
struct SOCKETINFO
{
	WSAOVERLAPPED overlapped;
	SOCKET sock;
	char buf[MIN_BUFFER + 1];
	int recvbytes;
	int sendbytes;
	WSABUF wsabuf;
	int EventType;
};

constexpr float sizeX = (800 / 8);
constexpr float sizeY = (600 / 8);
