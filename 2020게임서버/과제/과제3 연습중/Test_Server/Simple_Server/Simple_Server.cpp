#include <iostream>
#include <WS2tcpip.h>				// 윈도우 소켓 사용
#include "protocol.h"
using namespace std;

#pragma comment(lib, "Ws2_32.lib")	

// 키입력 확인용
const DWORD KEY_UP = 0x00000001;
const DWORD KEY_DOWN = 0x00000002;
const DWORD KEY_LEFT = 0x00000004;
const DWORD KEY_RIGHT = 0x00000008;
const DWORD KEY_ESCAPE = 0x00000010;


int nTotalSockets = 0;
SOCKETINFO* SocketInfoArray[MAX_USER];
WSAEVENT EventArray[MAX_USER];
CRITICAL_SECTION cs;

// 비동기 입출력 처리 함수
DWORD WINAPI WorkerThread(LPVOID arg);

// 소켓 관리 함수
BOOL AddSocketInfo(SOCKET sock);
void RemoveSocketInfo(int nIndex);

// 오류 출력 함수
void error_display(const char* msg, int err_no)
{
	WCHAR* lpMsgBuf;
	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, err_no, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPWSTR)&lpMsgBuf, 0, NULL);
	cout << msg;
	wcout << L"  에러 => " << lpMsgBuf << endl;
	while (true);
	LocalFree(lpMsgBuf);
}

int main()
{
	wcout.imbue(std::locale("korean"));
	InitializeCriticalSection(&cs);
	// 윈속 초기화
	WSADATA WSAData;
	WSAStartup(MAKEWORD(2, 0), &WSAData);	// WSACleanup과 쌍을 이룸, 윈속 사용을 알려주는 것, 무조건 이렇게 한다 그냥

	SOCKET listenSocket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);		// 서버와 연결하는 소켓 생성

	// bind
	SOCKADDR_IN serverAddr;		// 서버 주소 선언
	memset(&serverAddr, 0, sizeof(SOCKADDR_IN));	// 반드시 모두 0으로 초기화 해줘야한다. 
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(SERVER_PORT);
	serverAddr.sin_addr.s_addr = INADDR_ANY;
	::bind(listenSocket, (sockaddr*)&serverAddr, sizeof(serverAddr));
	listen(listenSocket, 5);

	// 더미 이벤트 객체 생성
	WSAEVENT hEvent = WSACreateEvent();
	EventArray[nTotalSockets++] = hEvent;

	// 쓰레드 생성
	HANDLE hThread = CreateThread(NULL, 0, WorkerThread, NULL, 0, NULL);
	if (hThread == NULL)
		return 1;

	// 데이터 통신에 사용할 변수
	SOCKET client_sock;
	SOCKADDR_IN client_addr;	
	int addrlen;
	DWORD recvbytes, flags;

	while (true)
	{
		// accept()
		addrlen = sizeof(client_addr);
		client_sock = accept(listenSocket, (sockaddr*)&client_addr, &addrlen);
		if (client_sock == INVALID_SOCKET)
		{
			error_display("accpet()",0);
			break;
		}

		cout << "Client 접속" << endl;

		// 소켓 정보 추가
		if (AddSocketInfo(client_sock) == FALSE)
		{
			closesocket(client_sock);
			continue;
		}

		// 비동기 입출력 시작
		SOCKETINFO* ptr = SocketInfoArray[nTotalSockets - 1];
		flags = 0;
		int retval = WSARecv(ptr->sock, &ptr->wsabuf, 1, &recvbytes, &flags, &ptr->overlapped, NULL);
		if (retval == SOCKET_ERROR) {
			if (WSAGetLastError() != WSA_IO_PENDING)
			{
				error_display("WSARecv()", 0);
				RemoveSocketInfo(nTotalSockets - 1);
				continue;
			}
		}

		// 소켓 변화 알림
		WSASetEvent(EventArray[0]);
	}

	// 종료
	CloseHandle(hThread);

	closesocket(listenSocket);
	WSACleanup();
	DeleteCriticalSection(&cs);
}
// 비동기 입출력 처리 함수
DWORD WINAPI WorkerThread(LPVOID arg)
{
	int retval;

	while (1)
	{
		// 이벤트 객체 관찰
		DWORD index = WSAWaitForMultipleEvents(nTotalSockets, EventArray, FALSE, WSA_INFINITE, FALSE);
		if (index == WSA_WAIT_FAILED)
			continue;
		index -= WSA_WAIT_EVENT_0;
		WSAResetEvent(EventArray[index]);
		if (index == 0)
			continue;

		// 클라이언트 정보 얻기
		SOCKETINFO* ptr = SocketInfoArray[index];
		SOCKADDR_IN clientaddr;
		int addrlen = sizeof(clientaddr);
		getpeername(ptr->sock, (SOCKADDR*)&clientaddr, &addrlen);

		// 비동기 입출력 결과 확인
		DWORD cbTransferred, flags;
		retval = WSAGetOverlappedResult(ptr->sock, &ptr->overlapped, &cbTransferred, FALSE, &flags);
		if (retval == FALSE || cbTransferred == 0)
		{
			RemoveSocketInfo(index);
			continue;
		}

		// 데이터 전송량 갱신
		if (ptr->recvbytes == 0)
		{
			ptr->recvbytes = cbTransferred;
			ptr->sendbytes = 0;

			// 받은 데이터 출력
			ptr->buf[ptr->recvbytes] = '\0';
			cout << ptr->buf << endl;
		}
		else
		{
			ptr->sendbytes += cbTransferred;
		}

		if (ptr->recvbytes > ptr->sendbytes)
		{
			// 데이터 보내기
			ZeroMemory(&ptr->overlapped, sizeof(ptr->overlapped));
			ptr->overlapped.hEvent = EventArray[index];
			ptr->wsabuf.buf = ptr->buf + ptr->sendbytes;
			ptr->wsabuf.len = ptr->recvbytes = ptr->sendbytes;

			DWORD sendbytes;
			retval = WSASend(ptr->sock, &ptr->wsabuf, 1, &sendbytes, 0, &ptr->overlapped, NULL);
			if (retval == SOCKET_ERROR)
			{
				if (WSAGetLastError() != WSA_IO_PENDING)
					error_display("WSASend()",0);
				continue;

			}
		}
		else
		{
			ptr->recvbytes = 0;

			// 데이터 받기
			ZeroMemory(&ptr->overlapped, sizeof(ptr->overlapped));
			ptr->overlapped.hEvent = EventArray[index];
			ptr->wsabuf.buf = ptr->buf;
			ptr->wsabuf.len = MIN_BUFFER;

			DWORD recvbytes;
			flags = 0;
			retval = WSARecv(ptr->sock, &ptr->wsabuf, 1, &recvbytes, &flags, &ptr->overlapped, NULL);
			if (retval == SOCKET_ERROR)
			{
				if (WSAGetLastError() != WSA_IO_PENDING)
					error_display("WSARecv()", 0);
				continue;
			}
		}

	}
}

// 소켓 관리 함수
BOOL AddSocketInfo(SOCKET sock)
{
	EnterCriticalSection(&cs);
	if (nTotalSockets >= MAX_USER)
		return FALSE;

	SOCKETINFO* ptr = new SOCKETINFO;
	if (ptr == NULL)
		return FALSE;

	WSAEVENT hEvent = WSACreateEvent();
	if (hEvent == WSA_INVALID_EVENT)
		return FALSE;

	ZeroMemory(&ptr->overlapped, sizeof(ptr->overlapped));
	ptr->overlapped.hEvent = hEvent;
	ptr->sock = sock;
	ptr->recvbytes = ptr->sendbytes = 0;
	ptr->wsabuf.buf = ptr->buf;
	ptr->wsabuf.len = MIN_BUFFER;
	ptr->EventType = EVENT_EXIT;
	SocketInfoArray[nTotalSockets] = ptr;
	EventArray[nTotalSockets] = hEvent;
	++nTotalSockets;

	// 데이터 보내기
	DWORD sendbytes;
	int retval = WSASend(ptr->sock, &ptr->wsabuf, 1, &sendbytes, 0, &ptr->overlapped, NULL);
	if (retval == SOCKET_ERROR)
	{
		if (WSAGetLastError() != WSA_IO_PENDING)
			error_display("WSASend()", 0);
	}

	LeaveCriticalSection(&cs);
	return TRUE;
}

// 소켓 정보 삭제
void RemoveSocketInfo(int nIndex)
{
	EnterCriticalSection(&cs);

	SOCKETINFO* ptr = SocketInfoArray[nIndex];
	closesocket(ptr->sock);
	delete ptr;
	WSACloseEvent(EventArray[nIndex]);

	if (nIndex != (nTotalSockets - 1))
	{
		SocketInfoArray[nIndex] = SocketInfoArray[nTotalSockets - 1];
		EventArray[nIndex] = EventArray[nTotalSockets - 1];
	}
	--nTotalSockets;

	LeaveCriticalSection(&cs);
}