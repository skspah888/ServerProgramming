#include "pch.h"
#include "Sector.h"

Sector::Sector()
{
}

Sector::~Sector()
{
}

void Sector::Insert_Object(int id)
{
	sl.lock();
	set_Objects.emplace(id);
	sl.unlock();
	//cout << "Insert : " << id << endl;
}

void Sector::Delete_Object(int id)
{
	sl.lock();
	set_Objects.erase(id);
	sl.unlock();
}
