#pragma once

#include "GameObject.h"
#include <windows.h>  
#include <sqlext.h>

class DataManager
{
public:
	explicit DataManager();
	~DataManager();

public:
	SQLHSTMT hstmt = 0;
	SQLHDBC hdbc;
	SQLHENV henv;
	SQLRETURN retcode;

public:
    void show_error(SQLHANDLE hHandle, SQLSMALLINT hType, RETCODE RetCode);
	void Initialize();
	void Release();

public:
	bool Check_Login(GameObject& obj, const cs_packet_login* p);
	void Insert_Client(int level, int exp, int hp, char name[10], int id, int x, int y );
	void Load_Client(GameObject& obj, int id, char name[10]);
	void Update_Info(int level, int exp, int hp, char name[10]);
	void Update_Position(int x, int y, char name[20]);

};

