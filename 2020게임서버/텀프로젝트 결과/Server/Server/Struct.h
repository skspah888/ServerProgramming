#pragma once

#include <WS2tcpip.h>
#include <MSWSock.h>
#include <chrono>
#include "Define.h"

// Overlapped 구조체의 확장 [ GQCS로 오는 정보가 빈약하기 때문 ]
struct OVER_EX {
	WSAOVERLAPPED wsa_over;
	char	op_mode;
	WSABUF	wsa_buf;
	unsigned char iocp_buf[MAX_BUFFER];
	int		object_id;
};


// EventType
struct event_type {
	int obj_id;
	std::chrono::system_clock::time_point wakeup_time;
	int event_id;
	int target_id;

	constexpr bool operator < (const event_type& _Left) const
	{
		return (wakeup_time > _Left.wakeup_time);
	}
};