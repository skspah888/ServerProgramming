﻿#include "pch.h"
#include "GameObject.h"
#include "Sector.h"
#include "DataManager.h"

// 사용할 전역 변수
mutex id_lock;
GameObject g_clients[MAX_USER + NUM_NPC];
HANDLE h_iocp;

SOCKET g_lSocket;
OVER_EX g_accept_over;

priority_queue<event_type> timer_queue;
mutex timer_l;

// DB관리
DataManager DB_Manager;

// 전체 맵을 Sector 단위로 분할하여 배열로 관리 [ 800x800 을 SECTOR_LIMIT로 나눠서 ]
Sector g_Sectors[(WORLD_WIDTH / SECTOR_LIMIT) + 1][(WORLD_HEIGHT / SECTOR_LIMIT) + 1];

template <class T>
constexpr const T& clamp(const T& value, const T& min, const T& max)
{
	return value < min ? min : max < value ? max : value;
}

void Update_Sector(int id, int old_X, int old_Y, int new_X, int new_Y)
{
	// 이동 전 위치에 따른 Sector 위치 찾기 
	int Old_SectorX = old_X / SECTOR_LIMIT;
	int Old_SectorY = old_Y / SECTOR_LIMIT;

	// 이동 후 위치에 따른 Sector 위치 찾기
	int New_SectorX = new_X / SECTOR_LIMIT;
	int New_SectorY = new_Y / SECTOR_LIMIT;

	// 이동전,후의 섹터가 다를경우만 Update
	if ((Old_SectorX != New_SectorX) || (Old_SectorY != New_SectorY))
	{
		g_Sectors[Old_SectorX][Old_SectorY].Delete_Object(id);		// 이전 것 Erase
		g_Sectors[New_SectorX][New_SectorY].Insert_Object(id);		// 이후 것 Insert
	}
}

template <>
struct hash<pair<int, int>>
{
	size_t operator()(const pair<int, int>& t) const
	{
		return t.first ^ t.second;
	}
};
// 변경된 위치가 어느 섹터에 있고, 해당 위치에 있는 섹터를 전부 찾아서 그 섹터만 검사하게 한다.
void Search_Near_Sector(unordered_set<pair<int, int>>* pSet_Position, int New_X, int New_Y)
{
	// 내 위치가 섹터 배열 중 어디에 있는지 찾아서 해당 섹터 모두 등록 ( set이 중복을 막아줌 )
	
	// 1. 현재 내 위치 등록
	pSet_Position->emplace((New_X / SECTOR_LIMIT), (New_Y / SECTOR_LIMIT));

	//cout << "내 위치 : " << New_X / SECTOR_LIMIT << " , " << (New_Y / SECTOR_LIMIT) << endl;

	// 2. 좌상단 등록
	int left_up_x, left_up_y;
	left_up_x = clamp<int>(New_X - VIEW_LIMIT, 0, WORLD_WIDTH);
	left_up_y = clamp<int>(New_Y - VIEW_LIMIT, 0, WORLD_HEIGHT);
	pSet_Position->emplace(left_up_x / SECTOR_LIMIT , left_up_y / SECTOR_LIMIT);

	//cout << "좌 상단: " << left_up_x / SECTOR_LIMIT << " , " << left_up_y / SECTOR_LIMIT << endl;


	// 3. 우상단
	int right_up_x, right_up_y;
	right_up_x = clamp<int>(New_X + VIEW_LIMIT, 0, WORLD_WIDTH);
	right_up_y = clamp<int>(New_Y - VIEW_LIMIT, 0, WORLD_HEIGHT);
	pSet_Position->emplace(right_up_x / SECTOR_LIMIT, right_up_y / SECTOR_LIMIT);


	//cout << "우 상단: " << right_up_x / SECTOR_LIMIT << " , " << (right_up_y / SECTOR_LIMIT) << endl;


	// 4. 좌하단
	int left_down_x, left_down_y;
	left_down_x = clamp<int>(New_X - VIEW_LIMIT, 0, WORLD_WIDTH);
	left_down_y = clamp<int>(New_Y + VIEW_LIMIT, 0, WORLD_HEIGHT);
	pSet_Position->emplace(left_down_x / SECTOR_LIMIT, left_down_y / SECTOR_LIMIT);

	//cout << "좌 하단: " << left_down_x / SECTOR_LIMIT << " , " << (left_down_y / SECTOR_LIMIT) << endl;

	// 5. 우하단
	int right_down_x, right_down_y;
	right_down_x = clamp<int>(New_X + VIEW_LIMIT, 0, WORLD_WIDTH);
	right_down_y = clamp<int>(New_Y + VIEW_LIMIT, 0, WORLD_HEIGHT);
	pSet_Position->emplace(right_down_x / SECTOR_LIMIT, right_down_y / SECTOR_LIMIT);

	//cout << "우 하단: " << right_down_x / SECTOR_LIMIT << " , " << (right_down_y / SECTOR_LIMIT) << endl;

}

// Timer Queue에 등록하기
void add_timer(int obj_id, int ev_type, std::chrono::system_clock::time_point t)
{
	event_type ev{ obj_id, t, ev_type };
	timer_l.lock();
	timer_queue.push(ev);
	timer_l.unlock();
}

void random_move_npc(int id);

// timer_thread에서 루프를 돌며 수행되는 주 업무
void time_worker()
{
	while (true)
	{
		while (true)
		{
			timer_l.lock();
			// timer_queue에 event가 들어가 있으면 ( 비어있지 않음 )
			if (false == timer_queue.empty())
			{
				event_type ev = timer_queue.top();							// 맨 위의 이벤트를 꺼냄
				if (ev.wakeup_time > std::chrono::system_clock::now())		// 해당 이벤트의 쿨타임 검사
				{
					timer_l.unlock();
					break;
				}

				// 쿨타임이 돌았으면 pop하고 해당 이벤트를 수행하기 위해 PQCS
				timer_queue.pop();
				timer_l.unlock();

				// 1. OP_RANDOM_MOVE 이벤트가 들어왔을때
				if (ev.event_id == OP_RANDOM_MOVE)
				{
					OVER_EX* ex_over = new OVER_EX;
					ex_over->op_mode = OP_RANDOM_MOVE;
					PostQueuedCompletionStatus(h_iocp, 1, ev.obj_id, &ex_over->wsa_over);
				}
				else if (ev.event_id == OP_PLAYER_ATTACK_CHECK)
				{
					OVER_EX* ex_over = new OVER_EX;
					ex_over->op_mode = OP_PLAYER_ATTACK_CHECK;
					PostQueuedCompletionStatus(h_iocp, 1, ev.obj_id, &ex_over->wsa_over);
				}
				else if (ev.event_id == OP_NPC_DIE)
				{
					OVER_EX* ex_over = new OVER_EX;
					ex_over->op_mode = OP_NPC_DIE;
					PostQueuedCompletionStatus(h_iocp, 1, ev.obj_id, &ex_over->wsa_over);
				}
			}
			else
			{
				timer_l.unlock();
				break;
			}
		}
		this_thread::sleep_for(1ms);
	}
}

// NPC의 상태를 is_active로 변경
void wake_up_npc(int id)
{
	if (!g_clients[id].is_active)
	{
		// is_active가 false이면 true로 변경
		bool bCheck = false;
		if (true == g_clients[id].is_active.compare_exchange_strong(bCheck, true))
		{
			add_timer(id, OP_RANDOM_MOVE, std::chrono::system_clock::now() + 1s);
		}
	}
}

// Error 메세지 출력
void error_display(const char* msg, int err_no)
{
	WCHAR* lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, err_no,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);
	std::cout << msg;
	std::wcout << L"에러 " << lpMsgBuf << std::endl;
	while (true);
	LocalFree(lpMsgBuf);
}

// id를 비교하여 NPC인가 확인
bool Is_NPC(int p1)
{
	return p1 >= MAX_USER;
}

// 거리를 비교하여 시야안에 있는지 확인
bool Is_Near(int p1, int p2)
{
	int dist = (g_clients[p1].x - g_clients[p2].x) * (g_clients[p1].x - g_clients[p2].x);
	dist += (g_clients[p1].y - g_clients[p2].y) * (g_clients[p1].y - g_clients[p2].y);

	return dist <= VIEW_LIMIT * VIEW_LIMIT;
}

// 인자로 받은 id를 가진 client에게 packet 전송
void send_packet(int id, void* p)
{
	unsigned char* packet = reinterpret_cast<unsigned char*>(p);
	OVER_EX* send_over = new OVER_EX;
	memcpy(send_over->iocp_buf, packet, packet[0]);
	send_over->op_mode = OP_MODE_SEND;
	send_over->wsa_buf.buf = reinterpret_cast<CHAR*>(send_over->iocp_buf);
	send_over->wsa_buf.len = packet[0];
	ZeroMemory(&send_over->wsa_over, sizeof(send_over->wsa_over));
	g_clients[id].c_lock.lock();
	if (true == g_clients[id].in_use)
		WSASend(g_clients[id].m_sock, &send_over->wsa_buf, 1,
			NULL, 0, &send_over->wsa_over, NULL);
	g_clients[id].c_lock.unlock();
}

// chat packet 생성 후 send_packet으로 client에 전송
void send_chat_packet(int to_client, int id, char* mess)
{
	sc_packet_chat p;
	p.id = id;
	p.size = sizeof(p);
	p.type = SC_PACKET_CHAT;
	strcpy_s(p.message, mess);
	send_packet(to_client, &p);
}

// login packet 생성 후 send_packet으로 client에 전송
void send_login_ok(int exp, int hp, int id, int level, int x, int y)
{
	sc_packet_login_ok p;
	/*p.exp = 0;
	p.hp = 100;
	p.id = id;
	p.level = 1;
	p.size = sizeof(p);
	p.type = SC_PACKET_LOGIN_OK;
	p.x = g_clients[id].x;
	p.y = g_clients[id].y;*/
	p.exp = exp;
	p.hp = hp;
	p.id = id;
	p.level = level;
	p.size = sizeof(p);
	p.type = SC_PACKET_LOGIN_OK;
	p.x = x;
	p.y = y;
	send_packet(id, &p);

}

// move packet 생성 후 send_packet으로 client에 전송
void send_move_packet(int to_client, int id)
{
	sc_packet_move p;
	p.id = id;
	p.size = sizeof(p);
	p.type = SC_PACKET_MOVE;
	p.x = g_clients[id].x;
	p.y = g_clients[id].y;
	p.move_time = g_clients[id].move_time;
	send_packet(to_client, &p);
}

// enter packet 생성 후 send_packet으로 client에 전송
void send_enter_packet(int to_client, int new_id)
{
	sc_packet_enter p;
	p.id = new_id;
	p.size = sizeof(p);
	p.type = SC_PACKET_ENTER;
	p.x = g_clients[new_id].x;
	p.y = g_clients[new_id].y;
	g_clients[new_id].c_lock.lock();
	strcpy_s(p.name, g_clients[new_id].name);
	g_clients[new_id].c_lock.unlock();
	p.o_type = 0;
	send_packet(to_client, &p);
}

// leave packet 생성 후 send_packet으로 client에 전송
void send_leave_packet(int to_client, int new_id)
{
	sc_packet_leave p;
	p.id = new_id;
	p.size = sizeof(p);
	p.type = SC_PACKET_LEAVE;
	send_packet(to_client, &p);
}

// 모든 스텟 변경을 한 후 , send_packet으로 client에 전송
void send_stat_change_packet(int to_client)
{
	sc_packet_stat_change p;
	p.size = sizeof(p);
	p.type = SC_PACKET_STAT_CHANGE;
	p.hp = g_clients[to_client].hp;
	p.level = g_clients[to_client].level;
	p.exp = g_clients[to_client].exp;

	g_clients[to_client].c_lock.lock();
	DB_Manager.Update_Info(g_clients[to_client].level, g_clients[to_client].exp, g_clients[to_client].hp, g_clients[to_client].name);
	g_clients[to_client].c_lock.unlock();

	send_packet(to_client, &p);
}

void process_move(int id, char dir)
{
	g_clients[id].last_move_time = std::chrono::system_clock::now();		// 현재 시간 측정

	short old_x, old_y, new_x, new_y;		// 구좌표, 신좌표
	old_x = new_x = g_clients[id].x;
	old_y = new_y = g_clients[id].y;

	switch (dir)
	{
	case MV_UP: if (new_y > 0) new_y--; break;
	case MV_DOWN: if (new_y < (WORLD_HEIGHT - 1)) new_y++; break;
	case MV_LEFT: if (new_x > 0) new_x--; break;
	case MV_RIGHT: if (new_x < (WORLD_WIDTH - 1)) new_x++; break;
	default: cout << "Unknown Direction in CS_MOVE packet.\n";
		while (true);
	}

	// old_viewlist : 이동 전, 현재 자신의 시야 영역 안에 있는 객체들
	g_clients[id].vl.lock();
	unordered_set <int> old_viewlist = g_clients[id].view_list;
	g_clients[id].vl.unlock();

	// 이전좌표, 바뀐좌표를 비교하여 섹터 업데이트 하기 
	Update_Sector(id, old_x, old_y, new_x, new_y);		// 맞는 섹터배열에 Insert, Delete
	
	g_clients[id].x = new_x;
	g_clients[id].y = new_y;

	g_clients[id].vl.lock();
	DB_Manager.Update_Position(g_clients[id].x, g_clients[id].y, g_clients[id].name);
	g_clients[id].vl.unlock();

	send_move_packet(id, id);		// 자기 자신 업데이트

	unordered_set<pair<int, int>> NearSectors;
	NearSectors.reserve(MAX_SEARCH);				// 내위치포함 좌우상하 4방향 = 최대 5개
	Search_Near_Sector(&NearSectors, new_x, new_y);


	// new_viewlist : 이동 후, 자신의 시야 영역 안에 있는 객체들 [ 클라이언트 + NPC ]
	unordered_set <int> new_viewlist;
	//for (int i = 0; i < MAX_USER; ++i) {
	//	if (false == g_clients[i].in_use) continue;
	//	if (id == i) continue;
	//	if (true == Is_Near(id, i)) new_viewlist.insert(i);
	//}

	//// NPC도 검사하여 new_List에 추가하기
	//for (int i = MAX_USER; i < MAX_USER + NUM_NPC; ++i) 
	//{
	//	if (true == Is_Near(id, i))
	//	{
	//		new_viewlist.insert(i);				// new_List에 추가
	//		g_clients[i].vl.lock();
	//		g_clients[i].view_list.insert(id);	// npc의 viewList에 클라이언트 추가
	//		g_clients[i].vl.unlock();
	//		wake_up_npc(i);
	//	}
	//}


	// 이전에는 모든 클라를 검사했더라면, 지금은 Sector 안에 있는 것만 검사한다.
	for (auto& pos : NearSectors)		// NearSectors에는 내 위치를 기준으로 검사해야할 섹터들의 위치를 알 수있는 pos 가 있음
	{
		if (false == g_Sectors[pos.first][pos.second].set_Objects.empty())
		{
			for (auto& obj : g_Sectors[pos.first][pos.second].set_Objects)
			{
				// NPC가 아닐 경우
				if (false == Is_NPC(obj))
				{
					if (false == g_clients[obj].in_use) continue;
					if (id == obj) continue;
					if (true == Is_Near(id, obj)) new_viewlist.insert(obj);
				}
				// NPC일 경우
				else
				{
					if (true == Is_Near(id, obj))
					{
						new_viewlist.insert(obj);				// new_List에 추가
						g_clients[obj].vl.lock();
						g_clients[obj].view_list.insert(id);	// npc의 viewList에 클라이언트 추가
						g_clients[obj].vl.unlock();
						wake_up_npc(obj);
					}
				}
			}
		}
	}

	// new_viewlist를 루프돌며 업데이트 
	// new_viewlist : 이동 후, 자신의 시야 영역 안에 있는 객체들 [ 클라이언트 + NPC ]
	for (int ob : new_viewlist)
	{
		// 1. 이전에는 시야에 없었는데, 이동후에는 시야에 있는 객체
		if (0 == old_viewlist.count(ob))
		{
			g_clients[id].vl.lock();
			g_clients[id].view_list.insert(ob);		// 클라이언트의 viewlist에 insert
			g_clients[id].vl.unlock();
			send_enter_packet(id, ob);				// enter를 하여 새로 들어왔음을 알림 [ 보이게 ]

			// 1-1. NPC가 아닐 경우, 해당 객체에게도 나의 존재를 알림
			if (false == Is_NPC(ob))
			{
				g_clients[ob].vl.lock();
				if (0 == g_clients[ob].view_list.count(id))
				{
					g_clients[ob].view_list.insert(id);
					g_clients[ob].vl.unlock();
					send_enter_packet(ob, id);
				}
				else
				{
					g_clients[ob].vl.unlock();
					send_move_packet(ob, id);
				}
			}
			// NPC일 경우 해당 NPC의 ViewList에 나를 추가한다.
			else
			{
				g_clients[ob].vl.lock();
				if (false == g_clients[ob].view_list.count(id))
				{
					g_clients[ob].view_list.insert(id);
					g_clients[ob].vl.unlock();
				}
				else
				{
					g_clients[ob].vl.unlock();
				}
			}
		}
		// 2. 이전에도 시야에 있었고, 이동후에도 시야에 있는 객체
		else 
		{  
			if (false == Is_NPC(ob))
			{
				g_clients[ob].vl.lock();
				if (0 != g_clients[ob].view_list.count(id))
				{
					g_clients[ob].vl.unlock();
					send_move_packet(ob, id);
				}
				else
				{
					g_clients[ob].view_list.insert(id);
					g_clients[ob].vl.unlock();
					send_enter_packet(ob, id);
				}
			}
		}
	}

	// old_viewlist를 루프돌며 업데이트
	for (int ob : old_viewlist)
	{
		// 1. 이전시야에는 있었는데, 새로운시야에서는 없어진 객체면
		if (0 == new_viewlist.count(ob))
		{
			g_clients[id].vl.lock();
			g_clients[id].view_list.erase(ob);			// 클라이언트의 viewList에서 지우고 leave 패킷 전송
			g_clients[id].vl.unlock();
			send_leave_packet(id, ob);

			// 상대 클라이언트의 ViewList에서도 나를 지움
			if (false == Is_NPC(ob))			
			{
				g_clients[ob].vl.lock();
				if (0 != g_clients[ob].view_list.count(id))
				{
					g_clients[ob].view_list.erase(id);
					g_clients[ob].vl.unlock();
					send_leave_packet(ob, id);
				}
				else
					g_clients[ob].vl.unlock();
			}
			// NPC일 경우, NPC의 ViewList에서 나를 지운다.
			else
			{
				g_clients[ob].vl.lock();
				if (0 < g_clients[ob].view_list.count(id))
				{
					g_clients[ob].view_list.erase(id);			
					g_clients[ob].vl.unlock();
				}
				else
				{
					g_clients[ob].vl.unlock();
				}
			}
		}
	}

	// 현재 id가 NPC가 아닐경우, new_viewlist에 있는 npc들에게 내가 움직였음을 알림
	if (false == Is_NPC(id)) {
		for (auto& npc : new_viewlist) {
			if (false == Is_NPC(npc)) continue;
			OVER_EX* ex_over = new OVER_EX;
			ex_over->object_id = id;
			ex_over->op_mode = OP_PLAYER_MOVE_NOTIFY;
			PostQueuedCompletionStatus(h_iocp, 1, npc, &ex_over->wsa_over);
		}
	}
}


// 클라이언트에서 온 packet 작업 수행
void process_packet(int id)
{
	char p_type = g_clients[id].m_packet_start[1];
	switch (p_type)
	{
	case CS_LOGIN: /////////////////////// 로그인시
	{
		cs_packet_login* p = reinterpret_cast<cs_packet_login*>(g_clients[id].m_packet_start);

		// DB검색하여 맞는 ID가 있는지 확인
		if (true == DB_Manager.Check_Login(g_clients[id], p))
		{
			// 이미 있는 정보다
			g_clients[id].c_lock.lock();
			DB_Manager.Load_Client(g_clients[id], id, p->name);
			g_clients[id].c_lock.unlock();
			send_login_ok(g_clients[id].exp, g_clients[id].hp, id, g_clients[id].level, g_clients[id].x, g_clients[id].y);
		}
		else
		{
			// 처음 등록된 정보 => DB에 데이터 저장
			g_clients[id].c_lock.lock();
			strcpy_s(g_clients[id].name, p->name);
			DB_Manager.Insert_Client(1, 0, 100, g_clients[id].name, id, g_clients[id].x, g_clients[id].y);
			g_clients[id].c_lock.unlock();

			send_login_ok(0,100,id,1,g_clients[id].x, g_clients[id].y);
		}
		

		// 근처 플레이어 찾기
		unordered_set<pair<int, int>> NearSectors;
		NearSectors.reserve(MAX_SEARCH);
		Search_Near_Sector(&NearSectors, g_clients[id].x, g_clients[id].y);

		for (auto& pos : NearSectors)
		{
			if (false == g_Sectors[pos.first][pos.second].set_Objects.empty())
			{
				for (auto& obj : g_Sectors[pos.first][pos.second].set_Objects)
				{
					// NPC가 아닐경우 (플레이어) View_List 갱신
					if (false == Is_NPC(obj))
					{
						if (true == g_clients[obj].in_use)
						{
							if (obj != id)
							{
								if (false == Is_Near(obj, id)) continue;
								if (0 == g_clients[obj].view_list.count(id)) {
									g_clients[obj].vl.lock();
									g_clients[obj].view_list.insert(id);
									g_clients[obj].vl.unlock();
									send_enter_packet(obj, id);
								}
								if (0 == g_clients[id].view_list.count(obj)) {
									g_clients[obj].vl.lock();
									g_clients[id].view_list.insert(obj);
									g_clients[obj].vl.unlock();
									send_enter_packet(id, obj);
								}
							}
						}
					}
					// NPC일 경우
					else
					{
						if (false == Is_Near(obj, id)) continue;
						g_clients[id].vl.lock();
						g_clients[id].view_list.insert(obj);		// 클라이언트의 ViewList안에 NPC (i) 추가
						g_clients[id].vl.unlock();

						g_clients[obj].vl.lock();
						g_clients[obj].view_list.insert(id);		// NPC (i)의 ViewList안에 클라이언트 (id) 추가
						g_clients[obj].vl.unlock();

						send_enter_packet(id, obj);
						wake_up_npc(obj);
					}
				}
			}
		}

		//// 근처에있는 클라이언트의 View List에 나를 추가, 나도 그 클라이언트를 추가 [ 로그인때 처음 한번하는 작업임 ]
		//for (int i = 0; i < MAX_USER; ++i)
		//	if (true == g_clients[i].in_use)
		//		if (id != i) {
		//			if (false == Is_Near(i, id)) continue;
		//			if (0 == g_clients[i].view_list.count(id)) {
		//				g_clients[i].vl.lock();
		//				g_clients[i].view_list.insert(id);
		//				g_clients[i].vl.unlock();
		//				send_enter_packet(i, id);
		//			}
		//			if (0 == g_clients[id].view_list.count(i)) {
		//				g_clients[i].vl.lock();
		//				g_clients[id].view_list.insert(i);
		//				g_clients[i].vl.unlock();
		//				send_enter_packet(id, i);
		//			}
		//		}

		//// 근처에 있는 NPC도 ViewList에 추가하며 wake_up 시키기
		//for (int i = MAX_USER; i < MAX_USER + NUM_NPC; ++i) 
		//{
		//	if (false == Is_Near(id, i)) continue;
		//	g_clients[id].vl.lock();
		//	g_clients[id].view_list.insert(i);		// 클라이언트의 ViewList안에 NPC (i) 추가
		//	g_clients[id].vl.unlock();

		//	g_clients[i].vl.lock();
		//	g_clients[i].view_list.insert(id);		// NPC (i)의 ViewList안에 클라이언트 (id) 추가
		//	g_clients[i].vl.unlock();

		//	send_enter_packet(id, i);
		//	wake_up_npc(i);

		//}
		break;
	}

	case CS_MOVE: // 클라이언트에게서 이동 패킷이 전송됨
	{
		cs_packet_move* p = reinterpret_cast<cs_packet_move*>(g_clients[id].m_packet_start);
		g_clients[id].move_time = p->move_time;

		// 1초에 한번씩 이동
		if (std::chrono::system_clock::now() - g_clients[id].last_move_time > 0.1s)
			process_move(id, p->direction);

		break;
	}

	case CS_ATTACK:		// 클라이언트에게서 공격 패킷이 전송됨
	{
		cs_packet_attack* p = reinterpret_cast<cs_packet_attack*>(g_clients[id].m_packet_start);
		short x = g_clients[id].x;
		short y = g_clients[id].y;

		// 공격이 가능할 때만 공격하기
		if (g_clients[id].is_attack == false)
		{
			// old_viewlist : 이동 전, 현재 자신의 시야 영역 안에 있는 객체들
			g_clients[id].vl.lock();
			unordered_set <int> viewlist = g_clients[id].view_list;
			g_clients[id].vl.unlock();

			for (auto& obj : viewlist)
			{
				// viewlist에 있는 Object들중 공격이 가능한 경우
				if (true == g_clients[id].Is_Attack(g_clients[obj].x, g_clients[obj].y))
				{
					// 1. 상대hp를 내 공격력만큼 감소
					g_clients[obj].hp -= g_clients[id].attack_power * g_clients[id].level * 2;
					//cout << g_clients[id].name << "플레이어가 " << g_clients[obj].name << "에게 " << g_clients[id].attack_power * g_clients[id].level * 2<< "데미지를 입힘." << endl;

					// 2. 상대 hp가 0보다 작을때 [ 상대가 죽음 ]
					if (g_clients[obj].hp <= 0)
					{
						int exp = g_clients[id].exp += 50;													// 경험치 증가
						// 그 상대가 NPC이면
						if (true == Is_NPC(obj))
						{
							g_clients[obj].x = -1000;		// 안보이는 곳으로 보내기
							g_clients[obj].y = -1000;
							add_timer(obj, OP_NPC_DIE, std::chrono::system_clock::now() + 5s);		// 5초뒤 재 생성
							//cout << g_clients[id].name << "플레이어가 " << g_clients[obj].name << " 몬스터 처치, 경험치 " << exp << "획득" << endl;
						}
						// 상대가 Player이면
						else
						{
							g_clients[obj].x = rand() % PLAYER_RANDOM + 25;				// 리스폰 지역으로 이동
							g_clients[obj].y = rand() % PLAYER_RANDOM + 25;
							g_clients[obj].hp = 100 * g_clients[obj].level;		// MAX로 HP 채우기
							g_clients[obj].exp = g_clients[obj].exp / 2;		// 경험치 반깎
							//cout << g_clients[id].name << "플레이어가 " << g_clients[obj].name << " 플레이어를 처치, 경험치 " << exp << "획득" << endl;
							send_leave_packet(id, obj);
						}
					}

					// 3. 경험치 체크
					if (g_clients[id].exp >= g_clients[id].level * 100)		// 100 , 200 , 300 순
					{
						++g_clients[id].level;								// 레벨 증가
						g_clients[id].hp = g_clients[id].level * 100;		// 최대 hp 증가 및 회복
					}

					if (true == Is_NPC(obj))
					{
						send_stat_change_packet(id);
					}
					else
					{
						send_stat_change_packet(obj);
					}
				}
			}

			// 1초후에 해당 아이디가 공격할수 있게 하기
			//cout << "타이머 등록 : 현재 " << g_clients[id].is_attack << endl;
			bool bCheck = false;
			if( true == g_clients[id].is_attack.compare_exchange_strong(bCheck, true))
				add_timer(id, OP_PLAYER_ATTACK_CHECK, std::chrono::system_clock::now() + 1s);
			
		}
	}
	break;

	default: cout << "Unknown Packet type [" << p_type << "] from Client [" << id << "]\n";
		while (true);
	}
}

void process_recv(int id, DWORD iosize)
{
	unsigned char p_size = g_clients[id].m_packet_start[0];
	unsigned char* next_recv_ptr = g_clients[id].m_recv_start + iosize;
	while (p_size <= next_recv_ptr - g_clients[id].m_packet_start) {
		process_packet(id);
		g_clients[id].m_packet_start += p_size;
		if (g_clients[id].m_packet_start < next_recv_ptr)
			p_size = g_clients[id].m_packet_start[0];
		else break;
	}

	long long left_data = next_recv_ptr - g_clients[id].m_packet_start;

	if ((MAX_BUFFER - (next_recv_ptr - g_clients[id].m_recv_over.iocp_buf))
		< MIN_BUFFER) {
		memcpy(g_clients[id].m_recv_over.iocp_buf,
			g_clients[id].m_packet_start, left_data);
		g_clients[id].m_packet_start = g_clients[id].m_recv_over.iocp_buf;
		next_recv_ptr = g_clients[id].m_packet_start + left_data;
	}
	DWORD recv_flag = 0;
	g_clients[id].m_recv_start = next_recv_ptr;
	g_clients[id].m_recv_over.wsa_buf.buf = reinterpret_cast<CHAR*>(next_recv_ptr);
	g_clients[id].m_recv_over.wsa_buf.len = MAX_BUFFER -
		static_cast<int>(next_recv_ptr - g_clients[id].m_recv_over.iocp_buf);

	g_clients[id].c_lock.lock();
	if (true == g_clients[id].in_use) {
		WSARecv(g_clients[id].m_sock, &g_clients[id].m_recv_over.wsa_buf,
			1, NULL, &recv_flag, &g_clients[id].m_recv_over.wsa_over, NULL);
	}
	g_clients[id].c_lock.unlock();
}

// 전달받은 Socket을 사용하여 새로운 클라이언트 생성
void add_new_client(SOCKET ns)
{
	int i;
	id_lock.lock();
	for (i = 0; i < MAX_USER; ++i)
		if (false == g_clients[i].in_use) break;
	id_lock.unlock();

	if (MAX_USER == i) {
		cout << "Max user limit exceeded.\n";
		closesocket(ns);
	}
	else {
		//cout << "New Client [" << i << "] Accepted" << endl;
		g_clients[i].c_lock.lock();
		g_clients[i].in_use = true;
		g_clients[i].m_sock = ns;
		g_clients[i].name[0] = 0;
		g_clients[i].c_lock.unlock();

		g_clients[i].m_packet_start = g_clients[i].m_recv_over.iocp_buf;
		g_clients[i].m_recv_over.op_mode = OP_MODE_RECV;
		g_clients[i].m_recv_over.wsa_buf.buf
			= reinterpret_cast<CHAR*>(g_clients[i].m_recv_over.iocp_buf);
		g_clients[i].m_recv_over.wsa_buf.len = sizeof(g_clients[i].m_recv_over.iocp_buf);
		ZeroMemory(&g_clients[i].m_recv_over.wsa_over, sizeof(g_clients[i].m_recv_over.wsa_over));
		g_clients[i].m_recv_start = g_clients[i].m_recv_over.iocp_buf;

		/*g_clients[i].x = rand() % WORLD_WIDTH-1;
		g_clients[i].y = rand() % WORLD_HEIGHT-1;*/
		g_clients[i].x = rand() % PLAYER_RANDOM + 25;
		g_clients[i].y = rand() % PLAYER_RANDOM + 25;

	/*	g_Sectors[g_clients[i].x / SECTOR_LIMIT][g_clients[i].y / SECTOR_LIMIT].sl.lock();
		g_Sectors[g_clients[i].x / SECTOR_LIMIT][g_clients[i].y / SECTOR_LIMIT].set_Objects.emplace(i);
		g_Sectors[g_clients[i].x / SECTOR_LIMIT][g_clients[i].y / SECTOR_LIMIT].sl.unlock();*/

		g_Sectors[g_clients[i].x / SECTOR_LIMIT][g_clients[i].y / SECTOR_LIMIT].Insert_Object(i);


		CreateIoCompletionPort(reinterpret_cast<HANDLE>(ns), h_iocp, i, 0);
		DWORD flags = 0;
		int ret;
		g_clients[i].c_lock.lock();
		if (true == g_clients[i].in_use) {
			ret = WSARecv(g_clients[i].m_sock, &g_clients[i].m_recv_over.wsa_buf, 1, NULL,
				&flags, &g_clients[i].m_recv_over.wsa_over, NULL);
		}
		g_clients[i].c_lock.unlock();
		if (SOCKET_ERROR == ret) {
			int err_no = WSAGetLastError();
			if (ERROR_IO_PENDING != err_no)
				error_display("WSARecv : ", err_no);
		}
	}
	SOCKET cSocket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	g_accept_over.op_mode = OP_MODE_ACCEPT;
	g_accept_over.wsa_buf.len = static_cast<ULONG> (cSocket);
	ZeroMemory(&g_accept_over.wsa_over, sizeof(&g_accept_over.wsa_over));
	AcceptEx(g_lSocket, cSocket, g_accept_over.iocp_buf, 0, 32, 32, NULL, &g_accept_over.wsa_over);
}

// 해당 id를 가진 Clients 접속 끊기
void disconnect_client(int id)
{
	//// DB 저장
	//g_clients[id].c_lock.lock();
	//DB_Manager.Update_Info(g_clients[id].level, g_clients[id].exp, g_clients[id].hp, g_clients[id].name);
	//DB_Manager.Update_Position(g_clients[id].x, g_clients[id].y, g_clients[id].name);
	//g_clients[id].c_lock.unlock();

	/*string strname{ g_clients[id].name };
	cout << strname << "플레이어 저장 " << g_clients[id].x << " , " << g_clients[id].y << endl;*/


	// 섹터에서 지우기 [ 작업 수행 필요 ]
	g_Sectors[g_clients[id].x / SECTOR_LIMIT][g_clients[id].y / SECTOR_LIMIT].Delete_Object(id);

	// 근처 플레이어 찾기
	unordered_set<pair<int, int>> NearSectors;
	NearSectors.reserve(MAX_SEARCH);				
	Search_Near_Sector(&NearSectors, g_clients[id].x, g_clients[id].y);

	for (auto& pos : NearSectors)
	{
		if (false == g_Sectors[pos.first][pos.second].set_Objects.empty())
		{
			for (auto& obj : g_Sectors[pos.first][pos.second].set_Objects)
			{
				// NPC가 아닐경우 (플레이어) View_List 갱신
				if (false == Is_NPC(obj))
				{
					if (true == g_clients[obj].in_use)
					{
						if (obj != id)
						{
							g_clients[obj].vl.lock();
							if (0 != g_clients[obj].view_list.count(id)) 
							{
								g_clients[obj].view_list.erase(id);
								g_clients[obj].vl.unlock();

								send_leave_packet(obj, id);
							}
							else
								g_clients[obj].vl.unlock();
						}
					}
				}
			}
		}
	}



	//// 자기자신이 아니고, 현재 사용중인 모든 클라이언트들의 View_List에서 지우고, leave_packet보내기
	//for (int i = 0; i < MAX_USER; ++i) {
	//	if (true == g_clients[i].in_use)
	//		if (i != id) {
	//			if (0 != g_clients[i].view_list.count(id)) {
	//				g_clients[i].view_list.erase(id);
	//				send_leave_packet(i, id);
	//			}
	//		}
	//}

	g_clients[id].c_lock.lock();
	g_clients[id].in_use = false;
	g_clients[id].view_list.clear();
	closesocket(g_clients[id].m_sock);
	g_clients[id].m_sock = 0;
	g_clients[id].c_lock.unlock();

	//cout << "Disconnect Client [" << id << "] Accepted" << endl;
}


// 서버 작업 수행하는 주 쓰레드 [ 모든 작업이 거진 worker thread를 통해 수행된다 ]
void worker_thread()
{
	// 이 쓰레드(worker_thread)를 IOCP thread pool에 등록			=> GQCS
	// IOCP가 처리를 맡긴 I/O 완료 데이터를 꺼내서 그 데이터를 처리 => GQCS
	while (true)
	{
		DWORD io_size;
		int key;
		ULONG_PTR iocp_key;
		WSAOVERLAPPED* lpover;
		int ret = GetQueuedCompletionStatus(h_iocp, &io_size, &iocp_key, &lpover, INFINITE);
		key = static_cast<int>(iocp_key);
		if (!ret) {
			error_display("GQCS Error : ", WSAGetLastError());
		}

		OVER_EX* over_ex = reinterpret_cast<OVER_EX*>(lpover);
		switch (over_ex->op_mode)
		{
		case OP_MODE_ACCEPT:
			add_new_client(static_cast<SOCKET>(over_ex->wsa_buf.len));
			break;

		case OP_MODE_RECV:
			if (0 == io_size)
				disconnect_client(key);
			else {
				// cout << "Packet from Client [" << key << "]" << endl;
				process_recv(key, io_size);
			}
			break;

		case OP_MODE_SEND:
			delete over_ex;
			break;

		case OP_RANDOM_MOVE:
		{
			random_move_npc(key);
			//add_timer(key, OP_RANDOM_MOVE, system_clock::now() + 1s);
			break;
		}
		case OP_PLAYER_MOVE_NOTIFY:
		{
			g_clients[key].c_lock.lock();
			lua_getglobal(g_clients[key].L, "event_player_move");
			lua_pushnumber(g_clients[key].L, over_ex->object_id);
			lua_pcall(g_clients[key].L, 1, 1, 0);
			g_clients[key].c_lock.unlock();
			delete over_ex;

			//delete over_ex;
			break;
		}

		case OP_PLAYER_ATTACK_CHECK:
		{
			bool bCheck = true;
			if(false == g_clients[key].is_attack.compare_exchange_strong(bCheck, false))
				add_timer(key, OP_PLAYER_ATTACK_CHECK, std::chrono::system_clock::now() + 1s);

			delete over_ex;
		}
		break;

		case OP_NPC_DIE:		// 새로운 위치에 npc부활시키기
		{
			g_clients[key].x = rand() % (WORLD_WIDTH - (WORLD_WIDTH / 20 )) + (WORLD_WIDTH / 20);
			g_clients[key].y = rand() % (WORLD_HEIGHT - (WORLD_HEIGHT / 20)) + (WORLD_HEIGHT / 20);
			g_clients[key].hp = g_clients[key].level * 50;
			delete over_ex;
		}
		break;
			
		}
	}
}

int API_get_x(lua_State* L)
{
	int user_id = lua_tointeger(L, -1);
	lua_pop(L, 2);
	int x = g_clients[user_id].x;
	lua_pushnumber(L, x);
	return 1;
}

int API_get_y(lua_State* L)
{
	int user_id = lua_tointeger(L, -1);
	lua_pop(L, 2);
	int y = g_clients[user_id].y;
	lua_pushnumber(L, y);
	return 1;
}

int API_SendMessage(lua_State* L)
{
	int my_id = (int)lua_tointeger(L, -3);
	int user_id = (int)lua_tointeger(L, -2);
	char* mess = (char*)lua_tostring(L, -1);
	lua_pop(L, 3);
	send_chat_packet(user_id, my_id, mess);

	// 몬스터 공격
	if (g_clients[my_id].is_attack == false)
	{
		g_clients[user_id].hp -= g_clients[my_id].attack_power * g_clients[my_id].level;
		//cout << g_clients[my_id].name << " 몬스터가" << g_clients[user_id].name << " 플레이어에게 " << g_clients[my_id].attack_power * g_clients[my_id].level << "데미지를 입힘." << endl;

		// 상대 피가 0 이하
		if (g_clients[user_id].hp <= 0)
		{
			g_clients[user_id].x = rand() % PLAYER_RANDOM + 25;					// 리스폰 지역으로 이동
			g_clients[user_id].y = rand() % PLAYER_RANDOM + 25;
			g_clients[user_id].hp = 100 * g_clients[user_id].level;		// MAX로 HP 채우기
			g_clients[user_id].exp = 0;									// 경험치 초기화
			//cout << g_clients[my_id].name << " 몬스터가" << g_clients[user_id].name << " 플레이어를 죽임" << endl;
		}

		send_stat_change_packet(user_id);

		// 1초후에 해당 아이디가 공격할수 있게 하기
		//cout << "타이머 등록 : 현재 " << g_clients[id].is_attack << endl;
		bool bCheck = false;
		if (true == g_clients[my_id].is_attack.compare_exchange_strong(bCheck, true))
			add_timer(my_id, OP_PLAYER_ATTACK_CHECK, std::chrono::system_clock::now() + 1s);
	}

	return 0;
}

void Initialize_NPC()
{
	cout << "Initializing NPCs\n";
	for (int i = MAX_USER; i < MAX_USER + NUM_NPC; ++i)
	{
		/*g_clients[i].x = rand() % WORLD_WIDTH-50  + 50;
		g_clients[i].y = rand() % WORLD_HEIGHT-50  + 50;*/
		g_clients[i].x = rand() % (WORLD_WIDTH - (WORLD_WIDTH / 20)) + (WORLD_WIDTH / 20);
		g_clients[i].y = rand() % (WORLD_HEIGHT - (WORLD_HEIGHT / 20)) + (WORLD_HEIGHT / 20);
		char npc_name[50];
		sprintf_s(npc_name, "N%d", i);
		strcpy_s(g_clients[i].name, npc_name);
		g_clients[i].is_active = false;

		/*g_Sectors[g_clients[i].x / SECTOR_LIMIT][g_clients[i].y / SECTOR_LIMIT].sl.lock();
		g_Sectors[g_clients[i].x / SECTOR_LIMIT][g_clients[i].y / SECTOR_LIMIT].set_Objects.emplace(i);
		g_Sectors[g_clients[i].x / SECTOR_LIMIT][g_clients[i].y / SECTOR_LIMIT].sl.unlock();*/

		g_Sectors[g_clients[i].x / SECTOR_LIMIT][g_clients[i].y / SECTOR_LIMIT].Insert_Object(i);

		lua_State* L = g_clients[i].L = luaL_newstate();
		luaL_openlibs(L);

		int error = luaL_loadfile(L, "Random_Monster.lua");
		error = lua_pcall(L, 0, 0, 0);

		lua_getglobal(L, "set_uid");
		lua_pushnumber(L, i);
		lua_pcall(L, 1, 1, 0);
		lua_register(L, "API_SendMessage", API_SendMessage);
		lua_register(L, "API_get_x", API_get_x);
		lua_register(L, "API_get_y", API_get_y);
		/*lua_register(L, "API_get_move_count", API_get_move_count);
		lua_register(L, "API_set_move_count", API_set_move_count);*/
	}
	cout << "NPC initialize finished.\n";
}


void random_move_npc(int id)
{
	short old_x, old_y, new_x, new_y;		// 구좌표, 신좌표
	old_x = new_x = g_clients[id].x;
	old_y = new_y = g_clients[id].y;

	switch (rand() % 4)
	{
	case 0: if (new_x > 0) new_x--; break;
	case 1: if (new_x < (WORLD_WIDTH - 1)) new_x++; break;
	case 2: if (new_y > 0) new_y--; break;
	case 3: if (new_y < (WORLD_HEIGHT - 1)) new_y++; break;
	}

	g_clients[id].x = new_x;
	g_clients[id].y = new_y;


	g_clients[id].vl.lock();
	unordered_set <int> old_viewlist = g_clients[id].view_list;
	g_clients[id].vl.unlock();

	// 이동 후의 NPC 좌표를 토대로 근처의 Player를 new_viewList에 담는다.
	unordered_set <int> new_viewlist;
	//for (int i = 0; i < MAX_USER; ++i) {
	//	if (id == i) continue;
	//	if (false == g_clients[i].in_use) continue;
	//	if (true == Is_Near(id, i)) new_viewlist.insert(i);
	//}

	Update_Sector(id, old_x, old_y, new_x, new_y);


	// 근처 플레이어 찾기
	unordered_set<pair<int, int>> NearSectors;
	NearSectors.reserve(MAX_SEARCH);
	Search_Near_Sector(&NearSectors, new_x, new_y);

	for (auto& pos : NearSectors)
	{
		if (false == g_Sectors[pos.first][pos.second].set_Objects.empty())
		{
			for (auto& obj : g_Sectors[pos.first][pos.second].set_Objects)
			{
				if (false == Is_NPC(obj))
				{
					if (id == obj) continue;
					if (false == g_clients[obj].in_use) continue;
					if (true == Is_Near(id, obj)) new_viewlist.insert(obj);
				}
			}
		}
	}

	// Old View List : 이동 전에 근처에 있었던 플레이어들
	// New View List : 이동 후에 근처에 있는 플레이어들
	for (auto& pl : old_viewlist)
	{
		// old에도 있고, new에도 있었던 경우 ( 계속 시야안에 존재 )
		if (0 < new_viewlist.count(pl))
		{
			g_clients[pl].vl.lock();
			// 해당 클라이언트의 View List에 있었던 경우
			if (0 < g_clients[pl].view_list.count(id))
			{
				g_clients[pl].vl.unlock();
				send_move_packet(pl, id);
			}
			// 해당 클라이언트의 View List에 없었던 경우
			else
			{
				g_clients[pl].view_list.insert(id);
				g_clients[pl].vl.unlock();
				send_enter_packet(pl, id);
			}
		}
		// old에는 있지만, new에는 없는 경우 ( 시야밖을 벗어남 )
		else
		{
			// NPC의 View List에서 Erase 함
			g_clients[id].vl.lock();
			g_clients[id].view_list.erase(pl);
			g_clients[id].vl.unlock();

			g_clients[pl].vl.lock();
			if (0 < g_clients[pl].view_list.count(id))
			{
				g_clients[pl].view_list.erase(id);
				g_clients[pl].vl.unlock();
				send_leave_packet(pl, id);
			}
			else
			{
				g_clients[pl].vl.unlock();
			}
		}
	}

	// new_viewList : 이동 후 근처에 있던 플레이어
	for (auto& pl : new_viewlist)
	{
		//// new에는 있는데 old에는 없는 경우
		//if (false == old_viewlist.count(pl))
		//{
		//	g_clients[id].vl.lock();
		//	g_clients[id].view_list.insert(pl);			// NPC의 View List에 추가하기
		//	g_clients[id].vl.unlock();
		//	send_enter_packet(pl, id);

		//}

		g_clients[pl].vl.lock();
		if (0 == g_clients[pl].view_list.count(id))
		{
			if (0 == g_clients[pl].view_list.count(id))
			{
				g_clients[pl].view_list.insert(id);
				g_clients[pl].vl.unlock();
				send_enter_packet(pl, id);
			}
			else
			{
				g_clients[pl].vl.unlock();
				send_move_packet(pl, id);
			}
		}
		else
		{
			g_clients[pl].vl.unlock();
		}
	}

	//// new_viewList : 이동 후 근처에 있던 플레이어
	//for (auto pl : new_viewlist)
	//{
	//	g_clients[pl].vl.lock();
	//	if (0 == g_clients[pl].view_list.count(pl))
	//	{
	//		if (0 == g_clients[pl].view_list.count(id))
	//		{
	//			g_clients[pl].view_list.insert(id);
	//			g_clients[pl].vl.unlock();
	//			send_enter_packet(pl, id);
	//		}
	//		else
	//			g_clients[pl].vl.unlock();

	//		send_move_packet(pl, id);
	//	}
	//	else
	//	{
	//		g_clients[pl].vl.unlock();
	//	}
	//}


	// npc 주위에 아무 플레이어도 없다.
	if (true == g_clients[id].view_list.empty()) {
		g_clients[id].is_active = false;
	}
	else
	{
		for (auto& pc : g_clients[id].view_list)
		{
			OVER_EX* over_ex = new OVER_EX;
			over_ex->object_id = pc;
			over_ex->op_mode = OP_PLAYER_MOVE_NOTIFY;
			PostQueuedCompletionStatus(h_iocp, 1, id, &over_ex->wsa_over);
		}
		add_timer(id, OP_RANDOM_MOVE, std::chrono::system_clock::now() + 1s);

		//cout << "NPC KEY : " << id << " , 위치: (" << g_clients[id].x << "," << g_clients[id].y << ")  , 근처 플레이어 수: " << g_clients[id].view_list.size() << endl;
	}
}

void npc_ai_thread()
{
	while (true) {
		auto start_time = std::chrono::system_clock::now();
		for (int i = MAX_USER; i < MAX_USER + NUM_NPC; ++i)
			random_move_npc(i);
		auto end_time = std::chrono::system_clock::now();
		auto exec_time = end_time - start_time;
		//cout << "AI exec time = " << std::chrono::duration_cast<std::chrono::seconds>(exec_time).count() << "s\n";
		this_thread::sleep_for(1s - (end_time - start_time));
	}
}

int main()
{
	DB_Manager.Initialize();

	std::wcout.imbue(std::locale("korean"));
	for (auto& cl : g_clients)
		cl.in_use = false;

	// iocp 생성
	WSADATA WSAData;
	WSAStartup(MAKEWORD(2, 0), &WSAData);
	h_iocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, NULL, 0);
	g_lSocket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	CreateIoCompletionPort(reinterpret_cast<HANDLE>(g_lSocket), h_iocp, KEY_SERVER, 0);

	SOCKADDR_IN serverAddr;
	memset(&serverAddr, 0, sizeof(SOCKADDR_IN));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(SERVER_PORT);
	serverAddr.sin_addr.s_addr = INADDR_ANY;
	::bind(g_lSocket, (sockaddr*)&serverAddr, sizeof(serverAddr));
	listen(g_lSocket, 5);

	SOCKET cSocket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	g_accept_over.op_mode = OP_MODE_ACCEPT;
	g_accept_over.wsa_buf.len = static_cast<int>(cSocket);
	ZeroMemory(&g_accept_over.wsa_over, sizeof(&g_accept_over.wsa_over));
	AcceptEx(g_lSocket, cSocket, g_accept_over.iocp_buf, 0, 32, 32, NULL, &g_accept_over.wsa_over);

	// 여기서 NPC 초기화
	Initialize_NPC();

	// timer_thread 생성
	thread timer_thread{ time_worker };

	// worker_thread 생성
	vector<thread> worker_threads;
	for (int i = 0; i < 6; ++i)
		worker_threads.emplace_back(worker_thread);


	for (auto& th : worker_threads)
		th.join();

	timer_thread.join();

	DB_Manager.Release();

	// 소켓 닫고 리소스 정리
	closesocket(g_lSocket);
	WSACleanup();
}
