#include "pch.h"
#include "DataManager.h"

DataManager::DataManager()
{
}

DataManager::~DataManager()
{
}

void DataManager::show_error(SQLHANDLE hHandle, SQLSMALLINT hType, RETCODE RetCode)
{
    SQLSMALLINT iRec = 0;
    SQLINTEGER iError;
    WCHAR wszMessage[1000];
    WCHAR wszState[SQL_SQLSTATE_SIZE + 1];
    if (RetCode == SQL_INVALID_HANDLE) {
        wcout << L"Invalid handle!\n";
        return;
    }
    while (SQLGetDiagRec(hType, hHandle, ++iRec, wszState, &iError, wszMessage,
        (SQLSMALLINT)(sizeof(wszMessage) / sizeof(WCHAR)), (SQLSMALLINT*)NULL) == SQL_SUCCESS) {
        // Hide data truncated..
        if (wcsncmp(wszState, L"01004", 5)) {
            //wcout << L"[" << wszState << "] " << wszMessage << "(" << iError << ")" << endl;
            //fwprintf(stderr, L"[%5.5s] %s (%d)\n", wszState, wszMessage, iError);
        }
    }
}

void DataManager::Initialize()
{
    retcode = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &henv);

    // Set the ODBC version environment attribute  
    if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
        retcode = SQLSetEnvAttr(henv, SQL_ATTR_ODBC_VERSION, (SQLPOINTER*)SQL_OV_ODBC3, 0);

        // Allocate connection handle  
        if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
        {
            retcode = SQLAllocHandle(SQL_HANDLE_DBC, henv, &hdbc);

            // Set login timeout to 5 seconds  
            if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
            {
                SQLSetConnectAttr(hdbc, SQL_LOGIN_TIMEOUT, (SQLPOINTER)5, 0);

                // Connect to data source   // ID와 PASSWORD를 넣어줘야함
                retcode = SQLConnect(hdbc, (SQLWCHAR*)L"2020_fall", SQL_NTS, (SQLWCHAR*)NULL, SQL_NTS, NULL, SQL_NTS);

                // Allocate statement handle  
                if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
                    retcode = SQLAllocHandle(SQL_HANDLE_STMT, hdbc, &hstmt);
                }
                else
                    show_error(hstmt, SQL_HANDLE_STMT, retcode);
            }
        }
        else
            show_error(hstmt, SQL_HANDLE_DBC, retcode);
    }
    else
        show_error(hstmt, SQL_HANDLE_ENV, retcode);


    cout << "DataBase Access" << endl;
}

void DataManager::Release()
{
	SQLFreeHandle(SQL_HANDLE_STMT, hstmt);
	SQLFreeHandle(SQL_HANDLE_DBC, hdbc);
	SQLFreeHandle(SQL_HANDLE_ENV, henv);
}

bool DataManager::Check_Login(GameObject& obj, const cs_packet_login* p)
{
    // 데이터를 읽는 변수들
    SQLWCHAR szName[MAX_ID_LEN] = L"";
    SQLLEN cbName = 0;      // 콜백 필수

    // 1. p에 있는 아이디가 DB에 있는지 검사
    char query[255];
    sprintf_s(query, "EXEC Find_Client %s", p->name);
    string ss = query;
    wstring wstrFunction = L"";
    wstrFunction.assign(ss.begin(), ss.end());

    /*string strName{ p->name };
    string strFuncion = "EXEC Find_Client " + strName;
    wstring wstrFunction = L"";
    wstrFunction.assign(strFuncion.begin(), strFuncion.end());*/

    retcode = SQLExecDirect(hstmt, (SQLWCHAR*)wstrFunction.c_str(), SQL_NTS);

    if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {

        retcode = SQLBindCol(hstmt, 1, SQL_C_WCHAR, szName, MAX_ID_LEN, &cbName);

        for (int i = 0; ; i++) {
            retcode = SQLFetch(hstmt);
            if (retcode == SQL_ERROR || retcode == SQL_SUCCESS_WITH_INFO)
                show_error(hstmt, SQL_HANDLE_STMT, retcode);
            if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
            {

            }
            else
                break;
        }
        SQLCancel(hstmt);

        wstring s{ szName };

        // 등록된 아이디가 없으므로 새로 만들고, DB에 등록후 로그인
        if (s.length() == 0)
            return false;

        // 등록된 아이디가 있으므로, 등록된 정보로 로그인
        else
            return true;
    }


}

void DataManager::Insert_Client(int level, int exp, int hp, char name[10], int id, int x, int y)
{
    // 함수 프로시저를 사용하여 DB에 데이터 Update하기
    char query[255];
    sprintf_s(query, "EXEC Insert_Client %d, %s, %d, %d, %d, %d, %d", id, name, level, exp, hp, x, y);
    string ss = query;
    wstring wstrFunction = L"";
    wstrFunction.assign(ss.begin(), ss.end());

    /*string strFuncion = "EXEC Insert_Client " + to_string(id) + ", " + name + ", " + to_string(level) + ", " + to_string(exp) + ", "
        + to_string(hp) + ", " + to_string(x) + ", " + to_string(y) + ", ";
    wstring wstrFunction = L"";
    wstrFunction.assign(strFuncion.begin(), strFuncion.end());*/

    retcode = SQLExecDirect(hstmt, (SQLWCHAR*)wstrFunction.c_str(), SQL_NTS);
    // Process data  
    if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
        SQLCancel(hstmt);

    //cout << "Insert Client" << endl;
}

void DataManager::Load_Client(GameObject& obj, int id, char name[10])
{
    // 데이터를 읽는 변수들
    SQLWCHAR szName[MAX_ID_LEN] = L"";
    SQLINTEGER ID, LEVEL, HP, X, Y, EXP;
    SQLLEN cbName = 0, cbID = 0, cbLEVEL = 0, cbX = 0, cbY = 0, cbHP = 0, cbEXP = 0;       // 콜백 ( 필수 )

    // 함수 프로시저를 사용하여 DB에 데이터 Update하기
    //cout << name << " 의 정보를 찾습니다." << endl;
    char query[255];
    sprintf_s(query, "EXEC Select_ClientInfo %s", name);
    string ss = query;
    wstring wstrFunction = L"";
    wstrFunction.assign(ss.begin(), ss.end());

  /*  string strName = name;
    string strFuncion = "EXEC Select_ClientInfo " + strName;
    wstring wstrFunction = L"";
    wstrFunction.assign(strFuncion.begin(), strFuncion.end());*/

    retcode = SQLExecDirect(hstmt, (SQLWCHAR*)wstrFunction.c_str(), SQL_NTS);

    // Process data  
    if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) 
    {
        // Bind columns 1, 2, and 3  [ 위에 SELECT 한 순서대로 Bind 해야함 ]
        retcode = SQLBindCol(hstmt, 1, SQL_C_LONG, &ID, 100, &cbID);
        retcode = SQLBindCol(hstmt, 2, SQL_C_WCHAR, szName, 10, &cbName);
        retcode = SQLBindCol(hstmt, 3, SQL_C_LONG, &LEVEL, 100, &cbLEVEL);
        retcode = SQLBindCol(hstmt, 4, SQL_C_LONG, &EXP, 100, &cbEXP);
        retcode = SQLBindCol(hstmt, 5, SQL_C_LONG, &HP, 100, &cbHP);
        retcode = SQLBindCol(hstmt, 6, SQL_C_LONG, &X, 100, &cbX);
        retcode = SQLBindCol(hstmt, 7, SQL_C_LONG, &Y, 100, &cbY);

        // Fetch and print each row of data. On an error, display a message and exit.  
        for (int i = 0; ; i++) {
            retcode = SQLFetch(hstmt);
            if (retcode == SQL_ERROR || retcode == SQL_SUCCESS_WITH_INFO)
                show_error(hstmt, SQL_HANDLE_STMT, retcode);
            if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
            {
                //cout << " 정보를 불러오고 있습니다 " << endl;
            }
            else
                break;
        }
        SQLCancel(hstmt);


        string strName;
        wstring wstrName{ szName };
        strName.assign(wstrName.begin(), wstrName.end());
        strcpy_s(obj.name, strName.c_str());
        //sprintf_s(obj.name, "%s", szName);
        obj.level = LEVEL;
        obj.exp = EXP;
        obj.hp = HP;
        obj.x = X;
        obj.y = Y;

        //cout << "결과 : " << obj.name << ", " << X << " , " << Y << endl;
    }
    else
    {
        show_error(hstmt, SQL_HANDLE_STMT, retcode);
    }

    //cout << "Load Client" << endl;

}

void DataManager::Update_Info(int level, int exp, int hp, char name[10])
{
    // 함수 프로시저를 사용하여 DB에 데이터 Update하기
    char query[255];
    sprintf_s(query, "EXEC Update_ClientInfo %d, %d, %d, %s", level, exp, hp, name);
    string ss = query;
    wstring wstrFunction = L"";
    wstrFunction.assign(ss.begin(), ss.end());

    /*string strFuncion = "EXEC Update_ClientInfo " + to_string(level) + ", " + to_string(exp) + ", " + to_string(hp) + ", " + name;
    wstring wstrFunction = L"";
    wstrFunction.assign(strFuncion.begin(), strFuncion.end());*/

    retcode = SQLExecDirect(hstmt, (SQLWCHAR*)wstrFunction.c_str(), SQL_NTS);

    // Process data  
    if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
        SQLCancel(hstmt);

    //cout << "Update Info : " << name << ", " << exp << ", " << hp << endl;

}

void DataManager::Update_Position(int x, int y, char name[20])
{
    // 함수 프로시저를 사용하여 DB에 데이터 Update하기

    char query[255];
    sprintf_s(query, "EXEC Update_Position %d, %d, %s", x, y, name);
    string ss = query;
    wstring wstrFunction = L"";
    wstrFunction.assign(ss.begin(), ss.end());

    //string strFuncion = "EXEC Update_Position" + to_string(x) + ", " + to_string(y) + ", " + name;
    //wstring wstrFunction = L"";
    //wstrFunction.assign(strFuncion.begin(), strFuncion.end());

    retcode = SQLExecDirect(hstmt, (SQLWCHAR*)wstrFunction.c_str(), SQL_NTS);

    // Process data  
    if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
        SQLCancel(hstmt);

    //cout << "Update Position : " << name << ", " << x << ", " << y << endl;

}
