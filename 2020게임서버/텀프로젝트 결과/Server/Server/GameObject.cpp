#include "pch.h"
#include "GameObject.h"

GameObject::GameObject()
{
}

GameObject::~GameObject()
{
}

void GameObject::Initialize(int _hp, int _exp, int _level)
{
	hp = hp;
	exp = _exp;
	level = _level;
}

bool GameObject::Is_Attack(int target_x, int target_y)
{
	// 해당 id가 공격 가능한 범위 안에 있는지 확인
	int dist = (x - target_x) * (x - target_x);
	dist += (y - target_y) * (y - target_y);

	// 거리 1이하면 true
	return dist <= 2;
}
