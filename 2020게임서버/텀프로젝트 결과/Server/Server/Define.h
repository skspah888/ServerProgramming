#pragma once

constexpr int MAX_BUFFER = 4096;
constexpr int MIN_BUFFER = 1024;

// GetQueuedCompletionStatus를 위한 타입 선언
constexpr char OP_MODE_RECV = 0;
constexpr char OP_MODE_SEND = 1;
constexpr char OP_MODE_ACCEPT = 2;


constexpr char OP_RANDOM_MOVE = 3;
constexpr char OP_PLAYER_MOVE_NOTIFY = 4;
constexpr char OP_PLAYER_ATTACK_CHECK = 5;
constexpr char OP_NPC_DIE = 6;

// 유니크한 Server Key
constexpr int  KEY_SERVER = 1000000;

// 섹터를 얼만큼의 단위로 분할할 것인지 [ 보통 최대시야 x 2 ]
constexpr int SECTOR_LIMIT = VIEW_LIMIT * 2;	

// window 크기
constexpr int Window_Width_Half = 10;
constexpr int Window_Height_Half = 10;

// 최대 LIMIT 제한
constexpr int MAX_SEARCH = 5;

//
constexpr int PLAYER_RANDOM = 750;


