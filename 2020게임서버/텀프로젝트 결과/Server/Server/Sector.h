#pragma once
class Sector
{
public:
	explicit Sector();
	~Sector();

public:
	mutex					sl;					// 멀티쓰레드 sector연산시 Lock, unLock
	unordered_set<int>		set_Objects;		// 해당 섹터안에 있는 Object들

public:
	void	Insert_Object(int id);				// sector에 해당 아이디 추가
	void	Delete_Object(int id);				// sector에 해당 아이디 제거
};

