#pragma once

// 게임에서 사용할 Object들의 최상의 클래스
#include <chrono>

class GameObject
{
public:
	explicit GameObject();
	virtual ~GameObject();

public:
	mutex c_lock;					// 맴버변수 참조를 위한 Lock 
	char name[MAX_ID_LEN];			// 오브젝트 이름
	short x, y;						// 위치 좌표
	bool in_use;
	OVER_EX	m_recv_over;
	int move_time;					// move_packet에 전송될 시간 저장
	std::chrono::system_clock::time_point last_move_time;
	atomic_bool is_attack = false;

	// 기본 정보
	short hp  = 100;
	short level = 1;
	int   exp = 0;
	short attack_power = 10;
	


	mutex vl;
	unordered_set <int> view_list;	// 시야 처리


	// Client
public:
	SOCKET	m_sock;
	unsigned char* m_packet_start;
	unsigned char* m_recv_start;


	// NPC
public:
	lua_State* L;
	atomic_bool is_active;


	// Function
public:
	void Initialize(int _hp, int _exp, int _level);

	bool Is_Attack(int target_x, int target_y);
};

