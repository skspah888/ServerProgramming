////////////////////////////////////////// SQLBindCol 테스트
// SQLBindCol_ref.cpp  
// compile with: odbc32.lib  
#include <windows.h>  
#include <stdio.h>  
#include <sqlext.h>  
#include <iostream>
#include <string>

using namespace std;

#define NAME_LEN 50  
#define PHONE_LEN 60

#pragma comment(lib, "odbc32.lib")

/************************************************************************
/* HandleDiagnosticRecord : display error/warning information
/*
/* Parameters:
/* hHandle ODBC handle
/* hType Type of handle (SQL_HANDLE_STMT, SQL_HANDLE_ENV, SQL_HANDLE_DBC)
/* RetCode Return code of failing command
/************************************************************************/
void show_error(SQLHANDLE hHandle, SQLSMALLINT hType, RETCODE RetCode)
{
    SQLSMALLINT iRec = 0;
    SQLINTEGER iError;
    WCHAR wszMessage[1000];
    WCHAR wszState[SQL_SQLSTATE_SIZE + 1];
    if (RetCode == SQL_INVALID_HANDLE) {
        wcout << L"Invalid handle!\n";
        return;
    }
    while (SQLGetDiagRec(hType, hHandle, ++iRec, wszState, &iError, wszMessage,
        (SQLSMALLINT)(sizeof(wszMessage) / sizeof(WCHAR)), (SQLSMALLINT*)NULL) == SQL_SUCCESS) {
        // Hide data truncated..
        if (wcsncmp(wszState, L"01004", 5)) {
            wcout << L"[" << wszState << "] " << wszMessage << "(" << iError << ")" << endl;
            //fwprintf(stderr, L"[%5.5s] %s (%d)\n", wszState, wszMessage, iError);
        }
    }
}

void Insert(int id, char name[10], int level, int exp, int hp, int x, int y)
{
    // 함수 프로시저를 사용하여 DB에 데이터 Update하기

    /*string strFuncion = "EXEC Insert_Client " + to_string(id) + ", " + name + ", " + to_string(level) + ", " + to_string(exp) + ", "
        + to_string(hp) + ", " + to_string(x) + ", " + to_string(y);
    wstring wstrFunction = L"";
    wstrFunction.assign(strFuncion.begin(), strFuncion.end());

    wcout << wstrFunction  << endl;*/

    SQLRETURN retcode;
    SQLHSTMT hstmt = 0;

    /*wchar_t sql_data[255];
    char  buf[255];
    sprintf_s(buf, "INSERT INTO USER_DATA VALUES('%d', '%s', '%d', '%d', '%d', '%d', '%d')", id, name, level, exp, hp, x, y);
    MultiByteToWideChar(CP_UTF8, 0, buf, strlen(buf), sql_data, sizeof sql_data / sizeof * sql_data);
    sql_data[strlen(buf)] = '\0';*/

    wchar_t sql_data[255];
    char query[255];
    sprintf_s(query, "INSERT INTO USER_DATA VALUES " "('%d', '%s', '%d', '%d', '%d', '%d', '%d')", id, name, level, exp, hp, x, y);
    MultiByteToWideChar(CP_UTF8, 0, query, strlen(query), sql_data, sizeof sql_data / sizeof * sql_data);
    sql_data[strlen(query)] = '\0';
    retcode = SQLExecDirect(hstmt, sql_data, SQL_NTS);

    //retcode = SQLExecDirect(hstmt, (SQLWCHAR*)L"EXEC Insert_Client 77, qqq, 777, 777, 777, 777, 77", SQL_NTS);
    //retcode = SQLExecDirect(hstmt, (SQLWCHAR*)wstrFunction.c_str(), SQL_NTS);
    // Process data  
    if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
        SQLCancel(hstmt);

    cout << "Insert Client" << endl;
}

int main() {
    SQLHENV henv;
    SQLHDBC hdbc;
    SQLHSTMT hstmt = 0;
    SQLRETURN retcode;
    // 데이터를 읽는 변수들
    SQLWCHAR szName[NAME_LEN] = L"";
    SQLINTEGER ID, LEVEL;
    // 콜백 ( 필수 )
    SQLLEN cbName = 0, cbID = 0, cbLEVEL = 0;

    // 한글 제대로 보이기 위함
    //setlocale(LC_ALL, "korean");              // printf
    std::wcout.imbue(std::locale("korean"));    // cout

    // Allocate environment handle  
    retcode = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &henv);

    // Set the ODBC version environment attribute  
    if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
        retcode = SQLSetEnvAttr(henv, SQL_ATTR_ODBC_VERSION, (SQLPOINTER*)SQL_OV_ODBC3, 0);

        // Allocate connection handle  
        if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
            retcode = SQLAllocHandle(SQL_HANDLE_DBC, henv, &hdbc);

            // Set login timeout to 5 seconds  
            if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
                SQLSetConnectAttr(hdbc, SQL_LOGIN_TIMEOUT, (SQLPOINTER)5, 0);

                // Connect to data source   // ID와 PASSWORD를 넣어줘야함
                retcode = SQLConnect(hdbc, (SQLWCHAR*)L"2020_fall", SQL_NTS, (SQLWCHAR*)NULL, 0, NULL, 0);

                // Allocate statement handle  
                if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
                    retcode = SQLAllocHandle(SQL_HANDLE_STMT, hdbc, &hstmt);

                    // SELECT ID, NAME, USER_LEVEL 은 내가 DB에 만든 테이블에 있는 데이터
                    // FROM USER_DATA 는 내가 만든 DB 테이블 이름
                    // ORDER BY 2,1,3 은 적힌 순서대로 정렬 ( NAME -> ID -> USER_LEVEL )
                    //retcode = SQLExecDirect(hstmt, (SQLWCHAR*)L"SELECT ID, NAME, USER_LEVEL FROM USER_DATA ORDER BY 2, 1, 3", SQL_NTS);
                    //retcode = SQLExecDirect(hstmt, (SQLWCHAR*)L"SELECT Client_ID, Client_Name, Client_Level FROM USER_DATA", SQL_NTS);

                    //Insert(55, szname, 5565, 5655, 5655, 5565, 655);
             


                    // 스토어 프로시저 사용
                    char name[20] = "fff";
                    int id = 3;
                    int level = 99;
                    int exp = 99;
                    int hp = 99;
                    int x = 99;
                    int y = 99;

                    char query[255];
                    sprintf_s(query, "EXEC Update_Position %d, %d, %s", x, y, name);
                    string ss = query;
                    wstring wstrFunction = L"";
                    wstrFunction.assign(ss.begin(), ss.end());


                    ////sprintf_s(query, "EXEC Insert_Client " "('%d', '%s', '%d', '%d', '%d', '%d', '%d')", id, name, level, exp, hp, x, y);
                    //char query[255];
                    //sprintf_s(query, "EXEC Insert_Client %d, %s, %d, %d, %d, %d, %d", id, name, level, exp, hp, x, y);
                    //string ss = query;
                    //wstring wstrFunction = L"";
                    //wstrFunction.assign(ss.begin(), ss.end());
                    retcode = SQLExecDirect(hstmt, (SQLWCHAR*)wstrFunction.c_str(), SQL_NTS);


                    //MultiByteToWideChar(CP_UTF8, 0, query, strlen(query), sql_data, sizeof sql_data / sizeof * sql_data);
                    //sql_data[strlen(query)] = '\0';
                    //retcode = SQLExecDirect(hstmt, sql_data, SQL_NTS);

     //               string name = "Test";
     //               string strFuncion = "EXEC Find_Client " + name;
					//wstring wstrFunction = L"";
					//wstrFunction.assign(strFuncion.begin(), strFuncion.end());

					//wcout << wstrFunction << endl;

                    // 스토어 프로시저 사용하기
                    //retcode = SQLExecDirect(hstmt, (SQLWCHAR*)L"EXEC Find_Client Tes", SQL_NTS);
                    //retcode = SQLExecDirect(hstmt, (SQLWCHAR*)wstrFunction.c_str(), SQL_NTS);
                    //retcode = SQLExecDirect(hstmt, (SQLWCHAR*)L"EXEC select_highlevel 2", SQL_NTS);

                    if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {

                        // Bind columns 1, 2, and 3  [ 위에 SELECT 한 순서대로 Bind 해야함 ]
                        //retcode = SQLBindCol(hstmt, 1, SQL_C_LONG, &ID, 100, &cbID);
                        retcode = SQLBindCol(hstmt, 1, SQL_C_WCHAR, szName, NAME_LEN, &cbName);
                        //retcode = SQLBindCol(hstmt, 3, SQL_C_LONG, &LEVEL, 100, &cbLEVEL);

                        // Fetch and print each row of data. On an error, display a message and exit.  
                        for (int i = 0; ; i++) {
                            retcode = SQLFetch(hstmt);
                            if (retcode == SQL_ERROR || retcode == SQL_SUCCESS_WITH_INFO)
                                show_error(hstmt, SQL_HANDLE_STMT, retcode);
                            if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
                            {
                                //replace wprintf with printf
                                //%S with %ls
                                //warning C4477: 'wprintf' : format string '%S' requires an argument of type 'char *'
                                //but variadic argument 2 has type 'SQLWCHAR *'
                                //wprintf(L"%d: %S %S %S\n", i + 1, sCustID, szName, szPhone);  
                                //wcout << i + 1 << ": " << ID << ", " << szName << ", " << LEVEL << endl;
                                wcout << szName << endl;
                                wstring s{ szName };
                                cout << s.length() << endl;
                                //wprintf(L"%d: %d %s %d\n", i + 1, ID, szName, LEVEL);
                            }
                            else
                            {
                                wcout << szName << endl;
                                wstring s{ szName };
                                cout << s.length() << endl;
                                break;
                            }
                        }
                    }
                    else
                    {
                        show_error(hstmt, SQL_HANDLE_STMT, retcode);
                    }

                    // Process data  
                    if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
                        SQLCancel(hstmt);
                        SQLCloseCursor(hstmt);
                        SQLFreeHandle(SQL_HANDLE_STMT, hstmt);
                    }

                    SQLDisconnect(hdbc);
                }
                else
                {
                    show_error(hstmt, SQL_NTS, retcode);
                }


                SQLFreeHandle(SQL_HANDLE_DBC, hdbc);
            }
        }
        SQLFreeHandle(SQL_HANDLE_ENV, henv);

    }
}