#include <iostream>
#include <sdkddkver.h>
#include <unordered_map>
#include <boost/asio.hpp>

using boost::asio::ip::tcp;
using namespace std;

constexpr int PORT = 3500;

class session;

unordered_map <int, session> g_clients;
int g_client_id = 0;

// 다중 접속 서버
class session
{
    int my_id;
    tcp::socket socket_;
    enum { max_length = 1024 };
    char data_[max_length];

public:
    session() : socket_(nullptr)
    {
        cout << "Session Creation Error.\n";
    }
    session(tcp::socket socket, int id) : socket_(std::move(socket)), my_id(id)
    {
        do_read();
    }

    void do_read()
    {
        socket_.async_read_some(boost::asio::buffer(data_, max_length),
            [this](boost::system::error_code ec, std::size_t length)
            {
                if (ec) {
                    cout << "Disconnected Client [" << my_id << "]." << endl;
                    g_clients.erase(my_id);
                }
                else {

                    data_[length] = 0;
                    cout << "Client [" << my_id << "]: " << data_ << endl;
                    g_clients[my_id].do_write(length);
                }
            });
    }
    void do_write(std::size_t length)
    {
        boost::asio::async_write(socket_, boost::asio::buffer(data_, length),
            [this](boost::system::error_code ec, std::size_t /*length*/)
            {
                if (!ec)g_clients[my_id].do_read();
                else g_clients.erase(my_id);
            });
    }
};

void accept_callback(boost::system::error_code ec, tcp::socket& socket, tcp::acceptor& my_acceptor)
{
    int new_id = g_client_id++;
    cout << "New Client [" << new_id << "] connected.\n";
    g_clients.try_emplace(new_id, move(socket), new_id);

    my_acceptor.async_accept([&my_acceptor](boost::system::error_code ec, tcp::socket socket) {
        accept_callback(ec, socket, my_acceptor);
        });
}

int main(int argc, char* argv[])
{
    try
    {
        boost::asio::io_context io_context;     // io_context 생성

        tcp::acceptor my_acceptor{ io_context, tcp::endpoint(tcp::v4(), PORT) };        // IPV4로 Endpointer를 만들고 accpetor 생성

        cout << "Server started at port " << PORT << ".\n";

        // accept한 결과는 콜백함수로 전달되는데, 여기선 람다를 사용
        my_acceptor.async_accept([&my_acceptor](boost::system::error_code ec, tcp::socket socket) {
            accept_callback(ec, socket, my_acceptor);
            });

        // 멀티쓰레드로 하고싶으면 쓰레드를 만들어서 거기서 run을 돌린다 [ 대신 g_clients에 대한 Data Race를 없애야함 ]
        io_context.run();
    }
    catch (std::exception& e)
    {
        std::cerr << "Exception: " << e.what() << "\n";
    }
}