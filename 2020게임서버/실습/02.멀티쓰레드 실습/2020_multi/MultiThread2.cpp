#include <thread>
#include <iostream>
using namespace std;

// 10.20 멀티쓰레드2 수업
volatile int sync_data = 0;
volatile bool data_ready = false;

// 피터슨 알고리즘 
volatile int sum;
volatile bool flag[2] = { false, false };
volatile int victim;

// Err 체크
volatile int* bound;
volatile bool done = false;
int g_error;

void bounder()
{
	for (int i = 0; i < 25000000; ++i)
		*bound = -(1 + *bound);
	done = true;
}

void inspector()
{
	while (false == done)
	{
		int v = *bound;
		if ((v != 0) && (v != -1))
		{
			cout << hex << v << ", ";
			g_error++;
		}
	}
}

void p_lock(int my_id)
{
	int other = 1 - my_id;
	flag[my_id] = true;
	victim = my_id;
	atomic_thread_fence(memory_order_seq_cst);
	while ((true == flag[other]) && (victim == my_id));
}

void p_unlock(int my_id)
{
	flag[my_id] = false;
}

void worker(int my_id)
{
	for (int i = 0; i < 25000000; ++i) {
		p_lock(my_id);
		sum = sum + 2;
		p_unlock(my_id);
	}
}

void receiver()
{
	while (false == data_ready);
	cout << "I received [" << sync_data << "]" << endl;
}

void sender()
{
	sync_data = 999;
	data_ready = true;
}

int main()
{
	////// VS의 사기 
	/*thread r_th{ receiver };
	thread s_th{ sender };

	r_th.join();
	s_th.join();*/

	///// 피터슨 알고리즘을 사용한 lock, unlock 
	//thread th1{ worker, 0 };
	//thread th2{ worker, 1 };

	//th1.join();
	//th2.join();

	//cout << "Sum = " << sum << endl;

	//// 캐시 라인 문제
	int a[32];
	long long addr = reinterpret_cast<long long>(&a[31]);
	int offset = addr % 64;
	addr = addr - offset;
	addr = addr - 2;
	bound = reinterpret_cast<int*>(addr);
	*bound = 0;

	thread th1{ bounder };
	thread th2{ inspector };

	th1.join();
	th2.join();

	cout << "Erro = " << g_error << endl;
}


