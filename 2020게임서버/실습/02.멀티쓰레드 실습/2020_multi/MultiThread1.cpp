#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <chrono>

using namespace std;
using namespace chrono;

constexpr int MAX_THREAD = 32;
volatile int sum = 0;
volatile int sumArr[MAX_THREAD * 16];
mutex sum_lock;

void worker_thread(int t_id, int num_threads)
{
	// class의 this와 비슷한 개념 : thread의 this
	//std::cout << "Hello world from thread [" << this_thread::get_id() << "\n";
	//std::cout << "Hello world from thread [" << th_id << "\n";

	// 이렇게 하면 각 쓰레드가 얼만큼 작업을 진행하고 있는지 알 수 없다.
	/*volatile int local_sum = 0;
	for (int i = 0; i < 5000'0000 / num_threads; ++i)
		local_sum = local_sum + 2;

	sum_lock.lock();
	sum = sum + local_sum;
	sum_lock.unlock();*/

	for (int i = 0; i < 5000'0000 / num_threads; ++i)
		sumArr[t_id * 16] = sumArr[t_id * 16] + 2;

	/*sum_lock.lock();
	sum = sum + sumArr[t_id];
	sum_lock.unlock(); */

}

int main()
{
	constexpr int MAX_THREADS = 32;

	for (int num = 1; num <= MAX_THREADS; num = num * 2)
	{
		vector<thread> threads;
		sum = 0;
		for (int i = 0; i < num; ++i)
			sumArr[i * 16] = 0;

		auto start_t = high_resolution_clock::now();

		for (int i = 0; i < num; ++i)
		{
			//threads.push_back(thread(worker_thread));		// 불필요한 메모리 이동 발생
			threads.emplace_back(worker_thread, i, num);
		}

		for (auto& th : threads)
			th.join();


		auto end_t = high_resolution_clock::now();
		auto du = end_t - start_t;
		auto mill_du = duration_cast<milliseconds>(du).count();

		int t_sum = 0;
		for (int i = 0; i < num; ++i)
			t_sum = t_sum + sumArr[i * 16];

		cout << num << " threads,  ";
		cout << "Sum = " << sum << "   , exec_time = " << mill_du << endl;
		cout << "t_Sum = " << t_sum << "   , exec_time = " << mill_du << endl;

	}

}