#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <chrono>
#include <atomic>

using namespace std;
using namespace chrono;

constexpr int MAX_THREAD = 16;
volatile int sum = 0;
mutex sum_lock;

void worker_thread(int t_id, int num_threads)
{
	for (int i = 0; i < 5000'0000 / num_threads; ++i)
	{
		sum_lock.lock();
		sum = sum + 2;
		sum_lock.unlock();
	}
}

int main()
{
	constexpr int MAX_THREADS = 32;

	for (int num = 1; num <= MAX_THREADS; num = num * 2)
	{
		vector<thread> threads;
		sum = 0;
		auto start_t = high_resolution_clock::now();

		for (int i = 0; i < num; ++i)
		{
			//threads.push_back(thread(worker_thread));		// 불필요한 메모리 이동 발생
			threads.emplace_back(worker_thread, i, num);
		}

		for (auto& th : threads)
			th.join();


		auto end_t = high_resolution_clock::now();
		auto du = end_t - start_t;
		auto mill_du = duration_cast<milliseconds>(du).count();

		cout << num << " threads,  ";
		cout << "Sum = " << sum << "   , exec_time = " << mill_du << endl;
	}

}