#include <iostream>
#include <WS2tcpip.h>
#include <MSWSock.h>

#include "protocol.h"
using namespace std;
#pragma comment(lib, "Ws2_32.lib")
#pragma comment(lib, "MSWSock.lib")
constexpr int MAX_BUFFER = 4096;

constexpr char OP_MODE_RECV = 0;
constexpr char OP_MODE_SEND = 1;
constexpr char OP_MODE_ACCEPT = 2;

constexpr int KEY_SERVER = 1000000;			// 클라이언트 ID와 겹치면 안됨

struct OVER_EX
{
	WSAOVERLAPPED	wsa_over;
	char			op_mode;				// Send인지 Recv인지
	WSABUF			wsa_buf;
	unsigned char	iocp_buf[MAX_BUFFER];
};

struct client_info {
	// 컨텐츠 데이터
	int id;
	char name[MAX_ID_LEN];
	short x, y;

	// 서버에서 사용할 데이터
	bool			in_use;				// 현재 사용중인지 아닌지 판별
	SOCKET			m_sock;
	OVER_EX			m_recv_over;
	unsigned char*	m_packet_start;
	unsigned char*	m_recv_start;

};

// 전역변수 
client_info		g_clients[MAX_USER];			// Data Race
HANDLE			h_iocp;							// No Data Race : 쓰레드 만들기 전에 얻고, 읽기만 하고 쓰지않기 때문
SOCKET			g_listenSocket;					// No Data Race	: 쓰레드 만들기 전에 얻고, 읽기만 하고 쓰지않기 때문
OVER_EX			g_accept_over;					// No Data Race	: 여러개의 쓰레드가 얘를 동시에 엑세스 하지 않기 때문

// 에러 처리 함수
void error_display(const char* msg, int err_no) 
{ 
	WCHAR* lpMsgBuf; 
	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL, err_no, 
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR)&lpMsgBuf, 0, NULL); 
	std::cout << msg; 
	std::wcout << L"에러 " << lpMsgBuf << std::endl; 
	while (true); 
	LocalFree(lpMsgBuf); 
}

// 클라이언트에 패킷을 보내는 함수
void send_packet(int id, void* p)
{
	unsigned char* packet = reinterpret_cast<unsigned char*>(p);
	OVER_EX* send_over = new OVER_EX;
	memcpy(send_over->iocp_buf, packet, packet[0]);
	send_over->op_mode = OP_MODE_SEND;
	send_over->wsa_buf.buf = reinterpret_cast<char*>(send_over->iocp_buf);
	send_over->wsa_buf.len = packet[0];
	ZeroMemory(&send_over->wsa_over, sizeof(send_over->wsa_over));

	WSASend(g_clients[id].m_sock, &send_over->wsa_buf, 1,
		NULL, 0, &send_over->wsa_over, NULL);
}

void send_login_ok(int id)
{
	sc_packet_login_ok p;
	p.exp = 0;
	p.hp = 100;
	p.id = id;
	p.level = 1;
	p.size = sizeof(p);
	p.type = SC_PACKET_LOGIN_OK;
	p.x = g_clients[id].x;
	p.y = g_clients[id].y;

	send_packet(id, &p);
}

void send_move_packet(int to_client, int id)
{
	sc_packet_move p;
	p.id = id;
	p.size = sizeof(p);
	p.type = SC_PACKET_MOVE;
	p.x = g_clients[id].x;
	p.y = g_clients[id].y;

	send_packet(to_client, &p);
}

void send_enter_packet(int to_client, int new_id)
{
	sc_packet_enter p;
	p.id = new_id;
	p.size = sizeof(p);
	p.type = SC_PACKET_ENTER;
	p.x = g_clients[new_id].x;
	p.y = g_clients[new_id].y;
	strcpy_s(p.name, g_clients[new_id].name);
	p.o_type = 0;

	// to에게 new_id가 왔음을 알려야함
	send_packet(to_client, &p);
}

void send_leave_packet(int to_client, int new_id)
{
	sc_packet_leave p;
	p.id = new_id;
	p.size = sizeof(p);
	p.type = SC_PACKET_LEAVE;
	
	// to에게 new_id가 왔음을 알려야함
	send_packet(to_client, &p);
}

void process_move(int id, char dir)
{
	short x = g_clients[id].x;
	short y = g_clients[id].y;

	switch (dir)
	{
	case MV_UP:		if (y > 0) y--; break;
	case MV_DOWN:		if (y < WORLD_HEIGHT - 1) y++; break;
	case MV_LEFT:		if (x > 0) x--; break;
	case MV_RIGHT:	if (x < WORLD_WIDTH - 1) x++; break;
	default:			cout << "Unknown Direction in CS_MOVE packet" << endl;
		while (true);
	}

	g_clients[id].x = x;
	g_clients[id].y = y;

	// 다른 플레이어한테도 가르쳐 줘야한다.
	for (int i = 0; i < MAX_USER; ++i)
	{
		if (g_clients[id].in_use)
		{
			send_move_packet(i,id);			// i에게 id의 움직임을 보낸다
		}
	}
}

// 클라이언트에서 오는 요청을 처리하는 함수
void process_packet(int id)
{
	// 패킷이 있는 시작점 [ 첫번째 바이트는 사이즈, 두번째 바이트는 타입 ]
	char p_type = g_clients[id].m_packet_start[1];

	// 어떤 패킷이 왔나?
	switch (p_type)
	{
	case CS_LOGIN:
	{
		cs_packet_login* p = reinterpret_cast<cs_packet_login*>(g_clients[id].m_packet_start);
		strcpy_s(g_clients[id].name, p->name);
		send_login_ok(id);

		for (int i = 0; i < MAX_USER; ++i)
		{
			if (g_clients[id].in_use)
			{
				if (id != i)
				{
					send_enter_packet(i, id);		// 새로 들어온 아이디를 모든 유저들에게 알려준다.
					send_enter_packet(id, i);		// 모든 유저들의 정보를 새로 들어온 아이디에게 알려준다.
				}
			}
		}
		break;
	}

	case CS_MOVE:
	{
		cs_packet_move* p = reinterpret_cast<cs_packet_move*>(g_clients[id].m_packet_start);
		process_move(id, p->direction);
		break;
	}

	default:
		cout << "Unknown Packet type [" << p_type << "] from Client [" << id << "]" << endl;
		while (true);	// 에러 확인용
	}

}

constexpr int MIN_BUFF_SIZE = 1024;
// id로부터 iosize만큼의 데이터가 왔음
void process_recv(int id, DWORD iosize)
{
	// 데이터가 오는대로 차곡차곡 쌓아놓고, 앞에서부터 처리하기 위함
	unsigned char p_size = g_clients[id].m_packet_start[0];
	unsigned char* next_recv_ptr = g_clients[id].m_recv_start + iosize;
	while (p_size <= next_recv_ptr - g_clients[id].m_packet_start)		// 남아있는 데이터가 패킷사이즈보다 큰지 아닌지 검사
	{
		process_packet(id);	 // 클라이언트에서 들어온 요청을 처리하는 핵심 컨텐츠
		g_clients[id].m_packet_start += p_size;
		if (g_clients[id].m_packet_start < next_recv_ptr)
			p_size = g_clients[id].m_packet_start[0];
		else 
			break;
	}

	int left_data = next_recv_ptr - g_clients[id].m_packet_start;		// 남아있는 데이터 크기


	// 남아있는 버퍼의 크기가 MIN_BUFF보다 작으면 데이터를 땡긴다 ( 처음으로 ) [ 유사 링버퍼 ]
	if ((MAX_BUFFER- (next_recv_ptr - g_clients[id].m_recv_over.iocp_buf)) < MIN_BUFF_SIZE)
	{
		memcpy(g_clients[id].m_recv_over.iocp_buf, g_clients[id].m_packet_start, left_data);
		g_clients[id].m_packet_start = g_clients[id].m_recv_over.iocp_buf;
		next_recv_ptr = g_clients[id].m_packet_start + left_data;
	}

	DWORD recv_flag = 0;
	// 받는 시작점을 next_recv_ptr로 하지않아 사이즈가 0으로 전달되는 현상이 발생했었음
	// => 패킷의 위치가 어긋나는 일이 없게 함 
	g_clients[id].m_recv_start = next_recv_ptr;
	g_clients[id].m_recv_over.wsa_buf.buf = reinterpret_cast<char*>(next_recv_ptr);
	g_clients[id].m_recv_over.wsa_buf.len = MAX_BUFFER - (next_recv_ptr - g_clients[id].m_recv_over.iocp_buf);		// 최대버퍼에서 사용중인크기를 빼서 길이를 정함

	WSARecv(g_clients[id].m_sock, &g_clients[id].m_recv_over.wsa_buf,
		1, NULL, &recv_flag, &g_clients[id].m_recv_over.wsa_over, NULL);

}

void add_new_client(SOCKET ns)
{
	int i = 0;
	for (i = 0; i < MAX_USER; ++i) {
		if (!g_clients[i].in_use)
			break;
	}
	
	if (MAX_USER == i) 
	{
		cout << "Max user limit exceeded" << endl;
		closesocket(ns);
	} 
	else
	{
		// 클라이언트 정보 초기화
		cout << "New Client [" << i << "] Accpeted" << endl;
		g_clients[i].id = i;
		g_clients[i].in_use = true;
		g_clients[i].m_packet_start = g_clients[i].m_recv_over.iocp_buf;
		g_clients[i].m_recv_over.op_mode = OP_MODE_RECV;
		g_clients[i].m_recv_over.wsa_buf.buf = reinterpret_cast<char*>(g_clients[i].m_recv_over.iocp_buf);
		g_clients[i].m_recv_over.wsa_buf.len = sizeof(g_clients[i].m_recv_over.iocp_buf);
		ZeroMemory(&g_clients[i].m_recv_over.wsa_over, sizeof(g_clients[i].m_recv_over.wsa_over));
		g_clients[i].m_recv_start = g_clients[i].m_recv_over.iocp_buf;
		g_clients[i].m_sock = ns;
		g_clients[i].name[0] = 0;
		g_clients[i].x = rand() % WORLD_WIDTH;
		g_clients[i].y = rand() % WORLD_HEIGHT;
		// 소켓을 IOCP에 등록하지 않아서 클라이언트의 데이터를 받지 못하고있었음
		CreateIoCompletionPort(reinterpret_cast<HANDLE>(ns), h_iocp, i, 0);
		DWORD flags = 0;
		int ret = WSARecv(g_clients[i].m_sock, &g_clients[i].m_recv_over.wsa_buf, 1, NULL,
			&flags, &g_clients[i].m_recv_over.wsa_over, NULL);

		if (SOCKET_ERROR == ret)
		{
			// 겹친 I/O 작업이 진행중입니다. => 하나의 소켓에 대해 Overlapped IO 작업이 겹침
			int err_no = WSAGetLastError();
			if(ERROR_IO_PENDING != err_no)
				error_display("WSARecv Error => ", err_no);
		}

	}

	// 끊이지않고 클라이언트를 받기위해 다시 AcceptEX를 호출해 줘야함
	SOCKET cSocket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	g_accept_over.op_mode = OP_MODE_ACCEPT;
	g_accept_over.wsa_buf.len = cSocket;						// 사용하지 않는 wsabuf.len에 cSocket을 넣는다.
	ZeroMemory(&g_accept_over.wsa_over, sizeof(g_accept_over.wsa_over));

	// Receive용 overlapped 구조체는 클라이언트마다 하나씩 있고,
	// Accept용 overlapped 구조체는 서버마다 하나씩 둔다.
	AcceptEx(g_listenSocket, cSocket, g_accept_over.iocp_buf, 0, 32, 32, NULL, &g_accept_over.wsa_over);
}

void disconnect_client(int id)
{
	// 다른 클라이언트에게도 접속 종료를 알려줘야함
	for (int i = 0; i < MAX_USER; ++i)
	{
		if (g_clients[id].in_use)
		{
			if(i != id)
				send_leave_packet(i, id);
		}
	}
	g_clients[id].in_use = false;
}

int main()
{
	std::wcout.imbue(locale("korean"));

	for (auto& client : g_clients)
	{
		client.in_use = false;
	}

	WSADATA WSAData;
	WSAStartup(MAKEWORD(2, 0), &WSAData);
	h_iocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, NULL, 0);						// iocp Create
	g_listenSocket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);			// Flag 변경
	CreateIoCompletionPort(reinterpret_cast<HANDLE>(g_listenSocket), h_iocp, KEY_SERVER, 0);	// iocp 등록

	SOCKADDR_IN serverAddr;
	memset(&serverAddr, 0, sizeof(SOCKADDR_IN));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(SERVER_PORT);
	serverAddr.sin_addr.s_addr = INADDR_ANY;
	::bind(g_listenSocket, (sockaddr *)&serverAddr, sizeof(serverAddr));
	listen(g_listenSocket, 5);

	SOCKET cSocket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	g_accept_over.op_mode = OP_MODE_ACCEPT;
	g_accept_over.wsa_buf.len = cSocket;						// 사용하지 않는 wsabuf.len에 cSocket을 넣는다. [ Integer ] 
	ZeroMemory(&g_accept_over.wsa_over, sizeof(g_accept_over.wsa_over));

	// Receive용 overlapped 구조체는 클라이언트마다 하나씩 있고,
	// Accept용 overlapped 구조체는 서버마다 하나씩 둔다.
	AcceptEx(g_listenSocket, cSocket, g_accept_over.iocp_buf, 0, 32, 32, NULL, &g_accept_over.wsa_over);

	while (true) {
		DWORD io_size;
		ULONG_PTR key;
		WSAOVERLAPPED* lpover;
		// 커널에 있는 IOCP 자료구조 Queue에서 하나씩 꺼내 처리
		int ret = GetQueuedCompletionStatus(h_iocp, &io_size, &key, &lpover, INFINITE);		// 데이터 받기
		cout << "Completion Detected" << endl;

		// GetQueued했을때 에러가 되면서 완료가되어 계속 ACCEPT함 => Max user limit exceeded
		if (FALSE == ret)
		{
			error_display("GQCS Error => ", WSAGetLastError());
			// 에러 시스템 호출에 전달된 데이터 영역이 너무 작습니다. => AcceptEX의 데이터영역이 너무 작다 [ 16 => 32 변경 ]
		}

		// receive 했는데 iosize가 0이면 클라접속 종료 [ 정상적 ]
		// 클라이언트가 강제로 종료할시 receive 에러 [ 윈도우에서 알아서 처리해주는것 같음 ]

		OVER_EX* over_ex = reinterpret_cast<OVER_EX*>(lpover);
		switch (over_ex->op_mode)
		{
		case OP_MODE_ACCEPT:	// 클라이언트 객체 추가
			add_new_client(static_cast<SOCKET>(over_ex->wsa_buf.len));		// Accpet할때 len에 cSocket을 넣어 놨었기 때문
			break;

		case OP_MODE_RECV:		// 패킷 재조립 및 패킷 처리
			if (0 == io_size)
				disconnect_client(key);
			else
			{
				cout << "Packet from Client [" << key << "]" << endl;
				process_recv(key, io_size);		// 클라이언트에서 온 패킷을 처리하는 이 부분이 제일 복잡함
			}
			break;

		case OP_MODE_SEND:		// 
			delete over_ex;		// 버퍼를 따로 할당 받았으면 delete를 2개해야함
			break;
		}
	}

	closesocket(g_listenSocket);
	WSACleanup();
}
