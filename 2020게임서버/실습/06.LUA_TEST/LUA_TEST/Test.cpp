#include <iostream>

// extern : lua54.lib는 C로 선언되어있어서 C++에서는 호출이 안된다.
// 따라서 lua54.lib에 있는 함수는 C++로 컴파일 된게 아니라 C로 컴파일된 함수라는 것을 알려줘야한다.
extern "C" {
#include "include/lua.h"
#include "include/lauxlib.h"
#include "include/lualib.h"
}

#pragma comment(lib, "lua54.lib")
using namespace std;

int addnum_c(lua_State* L)
{
	int a = lua_tonumber(L, -2);		// 최근 다음값 꺼내기
	int b = lua_tonumber(L, -1);		// 최근값 꺼내기
	lua_pop(L, 3);						// 불필요한 정보 삭제
	int result = a + b;
	lua_pushnumber(L, result);
	return 1;
}

int main()
{
	lua_State* L = luaL_newstate();		// 루아 가상머신 만들기
	luaL_openlibs(L);					// lua의 표준 라이브러리 로딩
	// luaL_loadbuffer(L, lua_code, strlen(lua_code), "line");
	luaL_loadfile(L, "dragon_move.lua");
	int err = lua_pcall(L, 0, 0, 0);
	if (err) {
		cout << "Error: " << lua_tostring(L, -1);
		lua_pop(L, 1);
	}

	/////////////// 변수값을 읽어 저장하기
	//lua_getglobal(L, "pos_x");
	//int pos_x = lua_tonumber(L, -1);
	//lua_getglobal(L, "pos_y");
	//int pos_y = lua_tonumber(L, -1);		// 가상머신 맨 앞의 값을 읽는것 (-1 : 맨앞, -2 : 그 뒤 )
	//cout << "Dragons position is (" << pos_x << "," << pos_y << ")" << endl;
	//lua_pop(L, 2);	// 가상머신에서 우리가 꺼낸 2개를 지워라

	
	
	///////////// 함수를 골라서 호출하기
	//lua_getglobal(L, "plustwo");	// 함수 등록하기
	//lua_pushnumber(L, 5);			// 파라미터 넣기
	//lua_pcall(L, 1, 1, 0);			// 함수 실행하기
	//int result = lua_tonumber(L, -1);
	//cout << "Result is "<< result << endl;
	//lua_pop(L, 1);

	
	
	/////////////////// Lua에서 C함수 호출하기
	lua_register(L, "c_addnum", addnum_c);		// C함수 등록하기
	lua_getglobal(L, "addnum_lua");				// lua함수 stack에 추가
	lua_pushnumber(L, 100);
	lua_pushnumber(L, 200);						// 파라미터 stack에 추가
	lua_pcall(L, 2, 1, 0);
	int result = lua_tonumber(L, -1);
	lua_pop(L, 1);

	cout << "Result is " << result << endl;


	lua_close(L);
}