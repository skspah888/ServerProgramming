#include <iostream>
#include <WS2tcpip.h>
using namespace std;
#pragma comment(lib, "Ws2_32.lib")
#define MAX_BUFFER        1024
#define SERVER_PORT       3500

// Callback 함수가 알기 위해 전역변수로 선언
char messageBuffer[MAX_BUFFER];
WSABUF wsabuf;
SOCKET client_socket;


void CALLBACK recv_complete(DWORD err, DWORD bytes, LPWSAOVERLAPPED over, DWORD flags);

void CALLBACK send_complete(DWORD err, DWORD bytes, LPWSAOVERLAPPED over, DWORD flags)
{
	if (bytes > 0) 
		printf("TRACE - Send message : %s (%d bytes)\n", messageBuffer, bytes);
	else
	{
		closesocket(client_socket);
		return;
	}

	// ret이 -1 이면 에러
	wsabuf.len = MAX_BUFFER;
	ZeroMemory(over, sizeof(*over));
	int ret = WSARecv(client_socket, &wsabuf, 1, NULL, &flags, over, recv_complete);
}

// overlapped callback 함수 
void CALLBACK recv_complete(DWORD err, DWORD bytes, LPWSAOVERLAPPED over, DWORD flags)
{
	if (bytes > 0) {
		messageBuffer[bytes] = 0;		// 끝에 0을 집어넣어서 cout시 제대로 출력하기 위함
		cout << "TRACE - Receive message : " << messageBuffer << "(" << bytes << " bytes)\n";
	}
	else
	{
		closesocket(client_socket);
		return;
	}
	wsabuf.len = bytes;					// 받은 만큼만 보내기 위함 ( 원래는 MAX_BUFFER )
	ZeroMemory(over, sizeof(*over));	// overlapped 구조체 초기화
	// ret이 -1 이면 에러
	int ret = WSASend(client_socket, &wsabuf, 1, NULL, NULL, over, send_complete);	
}

int main()
{
	WSADATA WSAData;
	WSAStartup(MAKEWORD(2, 0), &WSAData);
	SOCKET listenSocket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);		// Flag 변경
	SOCKADDR_IN serverAddr;
	memset(&serverAddr, 0, sizeof(SOCKADDR_IN));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(SERVER_PORT);
	serverAddr.sin_addr.s_addr = INADDR_ANY;
	::bind(listenSocket, (sockaddr *)&serverAddr, sizeof(serverAddr));
	listen(listenSocket, 5);
	SOCKADDR_IN client_addr;

	// while을 벗어난 순간 데이터가 날아가기 때문에 지역변수로 사용하지 않는다.
	WSAOVERLAPPED overlapped;

	while (true) {
		int addr_size = sizeof(client_addr);
		// listenSocket이 Overlapped 소켓이면, 반환도 Overlapped 소켓임
		client_socket = accept(listenSocket, (sockaddr*)&client_addr, &addr_size);
		wsabuf.buf = messageBuffer;
		wsabuf.len = MAX_BUFFER;
		DWORD flags = 0;
		ZeroMemory(&overlapped, sizeof(overlapped));		// overlapped 초기화(필수)

		// Recv가 while 루프하면 계속 쌓이기 때문에 하면 안됨
		// ret이 -1 이면 에러
		int ret = WSARecv(client_socket, &wsabuf, 1, NULL, &flags, &overlapped, recv_complete);
	}
			
	closesocket(listenSocket);
	WSACleanup();
}
