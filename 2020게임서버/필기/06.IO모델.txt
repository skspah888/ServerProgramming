● I/O 모델이 필요한 이유
- 블럭킹 방지 : WSARecv 동안 서버는 대기 상태
- 비 규칙적인 입출력 관리 : 누가 데이터를 보낼 지 알 수 없음
- 다중 접속 관리 : 재사용 문제

● 게임 서버의 접속
- 정해지지 않은 동작 순서
- 상대적으로 낮은 접속당 bandwidth (효율적 자원관리 필요)

ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ
● Socket Thread
- 쓰레드 하나가 클라이언트 하나를 맡아서 처리
- 다중 소켓 처리 가능
- 과도한 쓰레드 개수로 인한 운영체제 오버헤드
  -> Thread Control Block, Stack 메모리

● Non-blocking I/O
- 데이터가 안오면 Recv를 기다리지않고 끝낸다.
- 단점 : Busy Waiting
  -> 모든 소켓의 recv를 돌아가면서 반복 호출해야함
  -> 잦은 시스템 Call에 따른 CPU 낭비 => 성능 저하

● Select 
- 어떤 클라이언트가 데이터를 보내는지 알아내기 위한 모델
- 소켓 배열을 인자로 넣어서 검사 [ 읽기, 쓰기, 에러 등 ]
- Socket의 개수가 많아질수록 성능 저하 증가 ( Linear search )

● WSAAsyncSelect [ 윈도우에서 만든 Select + 비동기 방식 ]
- 소켓을 윈도우에 등록, 데이터가 오면 윈도우 이벤트가 발생
- 서버에서 못씀, 클라이언트에서 많이 쓰임 
  -> 윈도우 메시지 큐 사용 ( 성능 느림 )

● WSAEventSelect
- WSAAsyncSelect가 윈도우 메세지로 알림을 받았다면, 이건 이벤트로 알림
- socket과 event의 배열을 만들어서, 함수의 리턴값으로 부터 socket 추출

ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ
● Overlapped I/O 모델  [ 비동기 I/O ]
- Windows에서 추가된 고성능 I/O 모델 (대용량 고성능 네트워크 서버 필수)
- Select와는 다르게 I/O요청을 먼저하고 I/O종료를 나중에 확인
  -> recv호출시 Buf가 커널에 등록됨 [ I/O 요청 ]
  -> 이벤트 수신으로 커널에 있는 Buf를 처리 [ I/O 종료 ]
- I/O 요청 후 기다리지 않고 다른 일을 할 수있음, 여러개 요청 가능

* SO_SNDBUF 옵션
- 위 옵션을 지정하고 buffsize = 0으로 하면, recv하는 Buf가 메모리가 직접적
  으로 참조한다 [ 복사하는 과정이없어서 오버헤드가 적음 ]

* Non-Blocking IO 와의 차이
- I/O 호출과 완료가 분리되어 있음
  -> Non-Blocking은 실패와 성공이 있고, 실패하면 끝, 서버/커널 따로 실행
  -> Overlapped는 실패가없고, 서버와커널이 동시에 실행됨

* Overlapped I/O 정리
- 여러개의 Recv, Send가 겹쳐서 실행함 [ 커널이 알아서 순서대로 보냄 ]
  -> 여러 소켓에 대한 동시 다발적 Recv, Send 가능
  -> 하나의 Socket은 하나의 Recv만 가능 ★

● Overlapped I/O 실습
1. 소켓 생성 (WSASocket)
- flag 옵션 : WSA_FLAG_OVERRLAPPED

2. 데이터 (WSARecv)
- Socket은 overlapped I/O Socket 이여야 함
- 받은 데이터의 크기를 NULL 로 지정한다. 
  -> 데이터를 직접 받지 않는다라고 알림
- 마지막 2개의 파라미터값을 제대로 넣기 [ 이것으로 overlapped 구분 ]
  -> WSAOVERLAPPED 구조체 : 0으로 초기화후 사용, 중복사용불가능★
        => 호출 완료 후 다시 초기화 후 재사용 [ 오브젝트 풀링? ] 
  -> lpCompletionRoutine : I/O가 완료됐을때 알려주기위한 함수
  -> 이게 0이 아니여야 overlapped로 동작

* 처리 순서 비교 [ overlapped구조체의 hevent 사용시 ]
- WSAEventSelect : WaitforMultipleObject > Recv -> Buf
- OverlappedEvent : Recv -> WaitforMultipleObject -> buf -> Result


● Overlapped I/O의 두가지 모델
- Overlapped I/O가 언제 종료되었는지를 프로그램이 알아야 함
1. Overlapped I/O Event 모델  : 구조체에 있는 event [ 28p ]
- 동접 64개의 제한으로 인해 네트워크 I/O보다는 파일 I/O에 사용

2. Overlapped I/O Callback 모델 : lpCompletionRoutine 함수 등록
- 이벤트 제한 개수 없음, I/O가 끝나면 등록한 함수가 호출됨

● Overlapped I/O를 이용한 MMOServer 다중접속
* 클라이언트 변경
- Avartar(나)와 PC(상대)가 존재
- Avartar 구분하는 여러 방법
  -> 옵션
  -> ID로 구분

* 서버 변경
- 여러 개의 Socket 관리
- 여러 개의 Client Object 관리
- 객체 상태 변화 => BroadCast <= ID 필요
  -> BroadCast : 모든 클라이언트에 알려주는 것 [ID를 통해 누가 움직였다]

* 프로토콜 정의 필요
- 종류: Object 추가/이동/삭제
- 구성: Size, Type, DATA(id, 좌표, Avatar여부)


● 데이터를 어떻게 주고 받아야하는가?
1. CS 모델 [ 1대1 ]
- 클라이언트는 단순한 좌표 전송
- 서버는 좌표에 따른 이동값 전송

2. 다중 서버 모델
- 클라이언트는 좌표 전송
- 서버는 ID를 필수로 보내야하며, Object의 추가/이동/삭제를 전송
  -> add : ID, 좌표  
  -> remove : ID
 

